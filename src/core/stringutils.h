#include <string>
#include <algorithm>
#include <fstream>

namespace StringUtils {

std::string repeated( const std::string & str, unsigned int times );

std::string trimmed( const std::string & str );

std::string simplified( const std::string & str );

#if defined C11
std::string fromBase64( const std::string & str );
#endif

std::string toBase64( const std::string & str );

#if defined C11
std::string fromHex( const std::string & str );
#endif

#if defined C11
std::string toHex( const std::string & str );
#endif
};

