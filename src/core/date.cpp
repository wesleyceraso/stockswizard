#include "date.h"
#include <ratio>
#include <ctime>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <iomanip>

typedef std::chrono::duration< int, std::ratio< 60 * 60 * 24 > > day_duration;
typedef std::chrono::duration< int, std::ratio< 60 * 60 * 24 * 7 > > week_duration;

const std::vector< std::string > daysName = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
const std::vector< std::string > monthsName = { "January", "February", "March", "April", "May", "June",
                                                "July", "August", "September", "October", "November", "December" };

Date::Date():
    m_timePoint(std::chrono::system_clock::now()),
    m_isValid(true)
{
}

Date::Date(const Date & rhs):
    m_timePoint(rhs.m_timePoint),
    m_isValid(rhs.m_isValid)
{
}

Date::Date( int year, int monthsInYear, int daysInMonth ):
    m_isValid( true )
{
    struct tm timeStruct;
    timeStruct.tm_year = year;
    //tm_mon - months since January
    timeStruct.tm_mon = monthsInYear - 1;
    timeStruct.tm_mday = daysInMonth;
    //information is not available
    timeStruct.tm_isdst = -1;

    time_t tt = mktime( &timeStruct );
    if ( tt == -1 )
    {
        m_isValid = false;
    }

    m_timePoint = std::chrono::system_clock::from_time_t( tt );
}

int Date::dayOfWeek() const
{
    time_t tt = std::chrono::system_clock::to_time_t( m_timePoint );
    struct tm * timeStruct = gmtime( &tt );
    return timeStruct->tm_wday;
}

int Date::dayOfMonth() const
{
    time_t tt = std::chrono::system_clock::to_time_t( m_timePoint );
    struct tm * timeStruct = gmtime( &tt );
    return timeStruct->tm_mday;
}

int Date::dayOfYear() const
{
    time_t tt = std::chrono::system_clock::to_time_t( m_timePoint );
    struct tm * timeStruct = gmtime( &tt );
    //tm_yday - days since January 1
    return timeStruct->tm_yday + 1;
}

int Date::daysSinceEpoch() const
{
    day_duration days = std::chrono::duration_cast< day_duration >( m_timePoint.time_since_epoch() );
    return days.count();
}

int Date::daysTo( const Date & date ) const
{
    day_duration days = std::chrono::duration_cast< day_duration >( date.m_timePoint - m_timePoint );
    return days.count();
}

int Date::weekOfMonth() const
{
    int currentWeekLastDay = dayOfMonth() + ( 6 - dayOfWeek() );

    return ( currentWeekLastDay / 7 ) + 1;
}

int Date::weekOfYear() const
{
    int currentWeekLastDay = dayOfYear() + ( 6 - dayOfWeek() );

    return ( currentWeekLastDay / 7 ) + 1;
}

int Date::weeksSinceEpoch() const
{
    week_duration weeks = std::chrono::duration_cast< week_duration >( m_timePoint.time_since_epoch() );
    return weeks.count();
}

int Date::weeksTo( const Date & date ) const
{
    week_duration weeks = std::chrono::duration_cast< week_duration >( date.m_timePoint - m_timePoint );
    return weeks.count();
}

int Date::monthOfYear() const
{
    time_t tt = std::chrono::system_clock::to_time_t( m_timePoint );
    struct tm * timeStruct = gmtime( &tt );
    //tm_mon - months since January
    return timeStruct->tm_mon + 1;
}

int Date::monthsSinceEpoch() const
{
    return yearsSinceEpoch() * 12 + monthOfYear();
}

int Date::monthsTo( const Date & date ) const
{
    time_t tt = std::chrono::system_clock::to_time_t( m_timePoint );
    struct tm * myStruct = gmtime( &tt );

    time_t theirTt = std::chrono::system_clock::to_time_t( date.m_timePoint );
    struct tm * theirStruct = gmtime( &theirTt );

    int months = theirStruct->tm_mon - myStruct->tm_mon;
    if ( myStruct->tm_mday > theirStruct->tm_mday )
    {
        --months;
    }

    return months;
}

int Date::year() const
{
    time_t tt = std::chrono::system_clock::to_time_t( m_timePoint );
    struct tm * timeStruct = gmtime( &tt );
    //tm_year is the year since 1900
    return timeStruct->tm_year + 1900;
}

int Date::yearsSinceEpoch() const
{
    time_t tt = std::chrono::system_clock::to_time_t( m_timePoint );
    struct tm * timeStruct = gmtime( &tt );
    //tm_year is the year since 1900
    //epoch 1970
    return timeStruct->tm_year - 70;
}

int Date::yearsTo( const Date & date ) const
{
    time_t tt = std::chrono::system_clock::to_time_t( m_timePoint );
    struct tm * myStruct = gmtime( &tt );

    time_t theirTt = std::chrono::system_clock::to_time_t( date.m_timePoint );
    struct tm * theirStruct = gmtime( &theirTt );

    int years = theirStruct->tm_year - myStruct->tm_year;
    if ( myStruct->tm_mon > theirStruct->tm_mon ||
         ( ( myStruct->tm_mon == theirStruct->tm_mon ) && ( myStruct->tm_mday > theirStruct->tm_mday ) ) )
    {
        --years;
    }

    return years;
}

bool Date::isValid() const
{
    return m_isValid;
}

bool Date::isLeapYear() const
{
    time_t tt = std::chrono::system_clock::to_time_t( m_timePoint );
    struct tm * timeStruct = gmtime( &tt );
    return timeStruct->tm_isdst == 1;
}

std::string Date::toString( std::string format ) const
{
    //cria regexp do dia (D+)
    //substitue por std::fill( "0" ) << setw( match.size ) << dia
    //if nao achou, procure por shortDay ou longDay

    //cria regexp do mes (M+)
    //substitue por std::fill( "0" ) << setw( match.size ) << mes
    //if nao achou, procure por shortMonth ou longMonth

    //cria regexp do ano (Y+)
    //substitue por std::fill( "0" ) << setw( match.size ) << ano
    std::stringstream ss;
    ss << std::setfill( '0' );
    ss << std::setw( 2 );
    ss << dayOfMonth();
    ss << std::setw( 0 );
    ss << "/";
    ss << std::setw( 2 );
    ss << monthOfYear();
    ss << std::setw( 0 );
    ss << "/";
    ss << std::setw( 4 );
    ss << year();

    return ss.str();
}

Date Date::fromString(const std::string & date, std::string format)
{
    time_t tt = std::time(0);
    struct tm * timeStruct = std::localtime(&tt);

    //cria regexp do dia (D+)
    //tm_dia = atoi( match )

    //cria regexp do mes (M+)
    //tm_mes = atoi( match )

    //cria regexp do ano (Y+)
    //tm_ano = atoi( match )

    timeStruct->tm_year = atoi(date.substr(0,4).c_str()) - 1900;
    timeStruct->tm_mon = atoi(date.substr(4,2).c_str()) - 1;
    timeStruct->tm_mday = atoi(date.substr(6,2).c_str());
    timeStruct->tm_isdst = -1;

    tt = mktime(timeStruct);

    Date newDate;
    if (tt == -1)
    {
        newDate.m_isValid = false;
    }
    newDate.m_timePoint = std::chrono::system_clock::from_time_t(tt);

    return newDate;
}

Date Date::currentDate()
{
    return Date();
}

std::string Date::longDayName( unsigned int day )
{
    if ( day > daysName.size() )
        return std::string();

    return daysName[ day ];
}

std::string Date::longMonthName( unsigned int month )
{
    if ( month > monthsName.size() )
        return std::string();

    return monthsName[ month ];
}

std::string Date::shortDayName( unsigned int day )
{
    if ( day > daysName.size() )
        return std::string();

    return daysName[ day ].substr( 0, 3 );
}

std::string Date::shortMonthName( unsigned int month )
{
    if ( month > monthsName.size() )
        return std::string();

    return monthsName[ month ].substr( 0, 3 );
}

Date & Date::operator =(const Date & rhs)
{
    m_timePoint = rhs.m_timePoint;
    m_isValid = rhs.m_isValid;
    return *this;
}

bool Date::operator<( const Date & rhs ) const
{
    return m_timePoint < rhs.m_timePoint;
}

bool Date::operator==( const Date & rhs ) const
{
    return m_timePoint == rhs.m_timePoint;
}
