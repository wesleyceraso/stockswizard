#ifndef RANGE_H
#define RANGE_H

#include <cmath>

template < class T >
class Range
{
public:
    class iterator {
        friend class Range;
    public:
        T operator*() const { return m_i; }
        const iterator & operator++() { m_i += m_step; return *this; }
        iterator operator++( int ) { iterator copy( *this ); m_i += m_step; return copy; }

        bool operator==( const iterator & other ) const { return m_i == other.m_i; }
        bool operator!=( const iterator & other ) const { return m_i != other.m_i; }

    protected:
        iterator( T start, T step ):
            m_i( start ),
            m_step( step )
        {}

    private:
        T m_i;
        T m_step;
    };

    explicit Range( T min, T max, T step = 1 ):
        m_min( min ),
        m_max( max ),
        m_step( step )
    {}

    iterator begin() const { return iterator( m_min ); }
    iterator end() const { return iterator( m_max ); }

    T min() const { return m_min; }
    T max() const { return m_max; }

    T length() const { return std::abs( difference() ); }
    T difference() const { return m_max - m_min; }

private:
    T m_min;
    T m_max;
    T m_step;
};

#endif // RANGE_H
