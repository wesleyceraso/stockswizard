#ifndef DATE_H
#define DATE_H

#include <string>
#include <chrono>

class Date
{
public:
    Date();
    Date(const Date &);
    explicit Date( int year, int monthsInYear = 1, int daysInMonth = 1 );

    int dayOfWeek() const;
    int dayOfMonth() const;
    int dayOfYear() const;
    int daysSinceEpoch() const;
    int daysTo( const Date & date ) const;

    int weekOfMonth() const;
    int weekOfYear() const;
    int weeksSinceEpoch() const;
    int weeksTo( const Date & date ) const;

    int monthOfYear() const;
    int monthsSinceEpoch() const;
    int monthsTo( const Date & date ) const;

    int year() const;
    int yearsSinceEpoch() const;
    int yearsTo( const Date & date ) const;

    bool isValid() const;
    bool isLeapYear() const;

    std::string toString( std::string format = std::string( "DD/MM/AAAA" ) ) const;
    static Date fromString( const std::string & date, std::string format = std::string( "DD/MM/AAAA" ) );

    static Date currentDate();

    static std::string longDayName( unsigned int day );
    static std::string longMonthName( unsigned int month );
    static std::string shortDayName( unsigned int day );
    static std::string shortMonthName( unsigned int month );

    Date & operator=(const Date & rhs);
    bool operator<(const Date & rhs) const;
    bool operator==(const Date & rhs) const;

private:
    std::chrono::system_clock::time_point m_timePoint;
    bool m_isValid;
};

#endif // DATE_H
