#ifndef UTILS_H
#define UTILS_H

#include <list>
#include <vector>

template <class T>
std::vector<T> toVector(const std::list< T > & list)
{
    std::vector<T> v;
    v.reserve(list.size());
    for ( const T & t: list)
    {
        v.push_back(t);
    }
    return v;
}

template <class T>
bool approximatelyEqual(const T & lhs, const T & rhs, double range)
{
    return ((lhs * (1 + range)) > rhs && (lhs * (1 - range)) < rhs);
}

template <class T>
std::list<T> subList(const std::list<T> & list, unsigned int pos, unsigned int n)
{
    std::list<T> sub;
    typename std::list< T >::const_iterator it = list.begin();
    while (pos--) ++it;
    while (n--) sub.push_back(*(it++));
    return sub;
}

template <class T>
std::vector<T> subVector(const std::vector<T> & vector, unsigned int pos, unsigned int n)
{
    std::vector<T> sub;
    for (int i=pos; i<(pos+n); ++i) sub.push_back(vector[i]);
    return sub;
}

#endif // UTILS_H
