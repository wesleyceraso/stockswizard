#include "stockanalysiswindow.h"
#include "../analysis/envelopanalyser.h"
#include "../../utils/utils.h"
#include <QPointF>

StockAnalysisWindow::StockAnalysisWindow(QObject *parent) :
    QObject(parent)
{
    load();
}

void StockAnalysisWindow::load()
{
    m_bmf.setContentFromFile("COTAHIST_A2013.TXT");

    TradingDay tDay = m_bmf.tradingDays().back();
    std::list<Quote> quotes = m_bmf.quotes(0, &tDay);

    m_upToDate = QDate::fromString(QString::fromStdString(tDay.date().toString()), "dd/MM/yyyy");

    for (const Quote & q: quotes)
    {
        Stock s = q.stock();
        QVariantMap map;
        map.insert("code", QString::fromStdString(s.code()));
        map.insert("name", QString::fromStdString(s.name()));
        map.insert("close", q.close());
        map.insert("open", q.open());
        map.insert("high", q.high());
        map.insert("low", q.low());
        map.insert("volume", q.negotiations());

        QVariantList candles;
        for (const Quote & q: m_bmf.quotes(&s))
        {
            QVariantMap map;
            map.insert("low", q.low());
            map.insert("high", q.high());
            map.insert("open", q.open());
            map.insert("close", q.close());
            map.insert("average", q.average());
            map.insert("volume", q.negotiations());
            map.insert("day", QString::fromStdString(q.tradingDay().date().toString()));
            candles.push_back(map);
        }
        map.insert("quotes", candles);

        m_stocks.append(map);
    }

    emit stocksChanged();
}

QVariantList StockAnalysisWindow::stocks() const
{
    return m_stocks;
}

QDate StockAnalysisWindow::upToDate() const
{
    return m_upToDate;
}

QVariantMap StockAnalysisWindow::getEnvelop(QString stockCode, int period) const
{
    Stock s = m_bmf.stock(stockCode.toStdString());
    auto e = EnvelopAnalyser::process(toVector(m_bmf.quotes(&s)), period);

    QVariantMap map;

    QVariantList mme;
    int i = period;
    for (price p: e.first)
    {
        mme.append(QPointF(i++, p));
    }
    map.insert("mme", mme);
    map.insert("increment", e.second);

    return map;
}
