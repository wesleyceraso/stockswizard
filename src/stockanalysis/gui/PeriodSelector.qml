import QtQuick 2.1
import "chart"
import "analysis/ExponentialMovingAverage.js" as Lib

ChartXY {
    id: root
    interactive: false

    readonly property int max: mapper.mapFrom(Qt.point(end.x, 0))[0].x
    readonly property int min: mapper.mapFrom(Qt.point(start.x, 0))[0].x

    property int maxStart: model.length
    property int minStart: 0

    Rectangle {
        id: start
        height: parent.height
        width: 1
        color: "black"
        x: mapper.mapTo(Qt.point(minStart, 0))[0].x;

        Rectangle {
            anchors.centerIn: parent
            height: parent.height / 1.5
            width: 6
            color: "gray"
            border.width: 1

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.SizeHorCursor
                drag {
                    target: start
                    axis: Drag.XAxis
                    minimumX: 0
                    maximumX: end.x - 1
                }
            }
        }
    }

    Rectangle {
        id: end
        height: parent.height
        width: 1
        color: "black"
        x: mapper.mapTo(Qt.point(maxStart, 0))[0].x;

        Rectangle {
            anchors.centerIn: parent
            height: parent.height / 1.5
            width: 6
            color: "gray"
            border.width: 1

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.SizeHorCursor
                drag {
                    target: end
                    axis: Drag.XAxis
                    minimumX: start.x + 1
                    maximumX: root.width
                }
            }
        }
    }

    function getAverageVector(model) {
        var r = new Array;
        model.forEach(function(q){
            r.push(q.average);
        });
        return r;
    }

    property var model: []
    property color lineColor: "black"
    property real lineWidth: 1

    onModelChanged: root.requestPaint();

    property ChartMapper mapper: ChartMapper {
        id: chartMapper
        horizontalAxis: root.bottomAxis
        verticalAxis: root.rightAxis
        viewport: root.viewport
    }

    QtObject {
        id: _internal
        property var model: root.model.slice(
                                Math.min(Math.max(mapper.horizontalAxis.min, 0), root.model.length),
                                Math.min(Math.max(mapper.horizontalAxis.max, 0), root.model.length));
        property var line: Lib.makePointsFrom(getAverageVector(model), 0);
        property var max_min: Lib.findMaxMinPoints(line, 5, 5);
    }

    topAxis {
        ticks: 0
        subTickSize: 0
    }
    bottomAxis {
        ticks: 0
        subTickSize: 0
        min: 0
        max: model.length
    }

    leftAxis {
        ticks: 0
        subTickSize: 0
    }
    rightAxis {
        ticks: 0
        subTickSize: 0
        min: _internal.max_min[0]
        max: _internal.max_min[1]
    }

    grid {
        visible: false
    }

    drawableChildren: [
        ChartPath {
            id: path
            model: _internal.line
            mapper: root.mapper
            lineColor: root.lineColor
            lineWidth: root.lineWidth
        }
    ]
}
