import QtQuick 2.1
import "chart"
import "analysis"
import "analysis/ExponentialMovingAverage.js" as Lib

ChartXY {
    id: root

    property var model: []
    property int period: 2
    property color blockColor: "green"
    property real blockWidth: 2

    onModelChanged: root.requestPaint();

    QtObject {
        id: _internal
        property var model: root.model.slice(
                                Math.min(Math.max(mapper.horizontalAxis.min, 0), root.model.length),
                                Math.min(Math.max(mapper.horizontalAxis.max, 0), root.model.length));
        property var history: Lib.tradingVolume(model);
        property var max_min: Lib.findMaxMinPoints(history, 5, 0);
    }

    property ChartMapper mapper: ChartMapper {
        id: chartMapper
        horizontalAxis: root.bottomAxis
        verticalAxis: root.rightAxis
        viewport: root.viewport
    }

    rightAxis {
        min: _internal.max_min[0]
        max: 0
    }

    drawableChildren: [
        ChartHistogram {
            id: macdFast
            model: _internal.history
            mapper: root.mapper
            blockColor: root.blockColor
            blockWidth: root.blockWidth
        },
        ChartLabel {
            labels: {
                var r = new Array;
                var p1 = new Object;
                p1.color = root.blockColor;
                p1.label = "Trading Volume";
                r.push(p1);
                return r;
            }

            mapper: root.mapper
        }
    ]
}
