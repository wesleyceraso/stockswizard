import QtQuick 2.1
import "chart"
import "analysis"
import "analysis/ExponentialMovingAverage.js" as Lib

ChartXY {
    id: root

    property var model: []
    property int period: 2
    property color lineColor: "red"
    property real lineWidth: 1

    property var line: Lib.forceIndex(root.model, root.period)
    property real yMax
    property real yMin

    onModelChanged: root.requestPaint();

    onLineChanged: {
        var lines = root.line;
        if (lines.length) {
            var _yMax = lines[0].y;
            var _yMin = lines[0].y;
            for (var i=1; i<lines.length; ++i) {
                _yMax = Math.max(_yMax, lines[i].y);
                _yMin = Math.min(_yMin, lines[i].y);
            }
            var aux = (_yMax - _yMin) * 0.05;
            _yMax += aux;
            _yMin -= aux;
            root.yMax = _yMax;
            root.yMin = _yMin;
        }
    }

    property ChartMapper mapper: ChartMapper {
        id: chartMapper
        horizontalAxis: root.bottomAxis
        verticalAxis: root.rightAxis
        viewport: root.viewport
    }

    drawableChildren: [
        ChartPath {
            id: macdFast
            model: root.line
            mapper: root.mapper
            lineColor: root.lineColor
            lineWidth: root.lineWidth
        },
        ChartLabel {
            labels: {
                var r = new Array;
                var p1 = new Object;
                p1.color = root.lineColor;
                p1.label = "Force Index";
                r.push(p1);
                return r;
            }

            mapper: root.mapper
        }
    ]
}
