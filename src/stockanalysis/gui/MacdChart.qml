import QtQuick 2.1
import "chart"
import "analysis"
import "analysis/ExponentialMovingAverage.js" as Lib

ChartXY {
    id: root

    property var model: []
    property int period1: 12
    property int period2: 26
    property int period3: 9
    property color fastLineColor: "orange"
    property real fastLineWidth: 1
    property color slowLineColor: "green"
    property real slowLineWidth: 1
    property color histogramColor: "steelblue"
    property real histogramWidth: 2

    property var lines: Lib.macd(root.model, root.period1, root.period2, root.period3)
    property real yMax
    property real yMin

    onModelChanged: root.requestPaint();

    onLinesChanged: {
        var l = root.lines[0].concat(root.lines[1].concat(root.lines[2]));
        if (l.length) {
            var _yMax = l[0].y;
            var _yMin = l[0].y;
            for (var i=1; i<l.length; ++i) {
                _yMax = Math.max(_yMax, l[i].y);
                _yMin = Math.min(_yMin, l[i].y);
            }
            var aux = (_yMax - _yMin) * 0.05;
            _yMax += aux;
            _yMin -= aux;
            root.yMax = _yMax;
            root.yMin = _yMin;
        }
    }

    property ChartMapper mapper: ChartMapper {
        id: chartMapper
        horizontalAxis: root.bottomAxis
        verticalAxis: root.rightAxis
        viewport: root.viewport
    }

    drawableChildren: [
        ChartHistogram {
            id: histogram
            model: root.lines[2];
            mapper: root.mapper
            blockColor: root.histogramColor
            blockWidth: root.histogramWidth
        },
        ChartPath {
            id: macdFast
            model: root.lines[0];
            mapper: root.mapper
            lineColor: root.fastLineColor
            lineWidth: root.fastLineWidth
        },
        ChartPath {
            id: macdSlow
            model: root.lines[1];
            mapper: root.mapper
            lineColor: root.slowLineColor
            lineWidth: root.slowLineWidth
        },
        ChartLabel {
            labels: {
                var r = new Array;

                var p2 = new Object;
                p2.color = root.fastLineColor;
                p2.label = "Macd";
                r.push(p2);

                var p1 = new Object;
                p1.color = root.slowLineColor;
                p1.label = "Signal";
                r.push(p1);

                var p3 = new Object;
                p3.color = root.histogramColor;
                p3.label = "Histogram"
                r.push(p3);
                return r;
            }

            mapper: root.mapper
        }

    ]
}
