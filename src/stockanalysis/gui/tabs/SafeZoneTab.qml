import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Styles 1.0
import QtQuick.Window 2.0
import QtGraphicalEffects 1.0
import "../"

Tab {
    id: tab
    property StockChart chart

    title: "StopLoss"

    anchors {
        margins: 1
    }

    sourceComponent: ScrollView {
        id: scrollView

        clip: true

        contentItem: Item {

            height: vSpacer.height + column.height + vSpacer.height
            width: scrollView.width

            VerticalSpacer {
                id: vSpacer
                height: 20
            }

            Column {
                id: column

                height: childrenRect.height
                anchors {
                    top: vSpacer.bottom
                    left: parent.left
                    right: parent.right
                    leftMargin: 20
                    rightMargin: 20
                }

                spacing: 20

                Column {
                    height: childrenRect.height
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    Label {
                        text: "SafeZone"
                        font.pixelSize: 14
                    }

                    VerticalSeparator {
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                    }
                }

                Repeater {
                    id: rep

                    property var comp: Qt.createComponent("../analysis/SafeZone.qml");

                    model: 2
                    delegate: Column {
                        id: deleg
                        spacing: 5
                        height: childrenRect.height
                        anchors {
                            left: parent.left
                            right: parent.right
                        }

                        property var item

                        Component.onCompleted: {
                            var obj = rep.comp.createObject(deleg);
                            obj.mapper = chart.mapper;
                            deleg.item = obj;
                            tab.chart.addItem(obj);
                        }

                        Binding { target: item; property: "model"; value: chart.model }
                        Binding { target: item; property: "mapper"; value: chart.mapper }
                        Binding { target: item; property: "visible"; value: checkVisible.checked }
                        Binding { target: item; property: "period"; value: periodSB.value }
                        Binding { target: item; property: "lineColor"; value: colorRect.color }
                        Binding { target: item; property: "lineWidth"; value: lineWidthSB.value }
                        Binding { target: item; property: "multiplier"; value: multiplierSB.value }

                        Row {
                            height: childrenRect.height
                            CheckBox { id: checkVisible; checked: false }
                            Label { text: "SafeZone #" + (index + 1) }
                        }

                        Item {
                            height: periodSB.implicitHeight
                            width: parent.width

                            Label {
                                id: lbl1
                                anchors {
                                    verticalCenter: periodSB.verticalCenter
                                    verticalCenterOffset: 2
                                    right: parent.left
                                    rightMargin: -70
                                }
                                text: "Period:"
                            }
                            SpinBox {
                                id: periodSB
                                width: 50

                                anchors {
                                    left: lbl1.right
                                    leftMargin: 5
                                }

                                value: (index + 1) * 5
                                maximumValue: tab.chart.model.length
                                minimumValue: 0
                                stepSize: 1
                                decimals: 0
                            }
                        }

                        Item {
                            height: Math.max(lbl2.implicitHeight, colorRect.height)
                            width: parent.width

                            Label {
                                id: lbl2
                                anchors {
                                    verticalCenter: colorRect.verticalCenter
                                    verticalCenterOffset: 2
                                    right: parent.left
                                    rightMargin: -70
                                }
                                text: "Color:"
                            }
                            Rectangle {
                                id: colorRect
                                height: 15
                                width: 15

                                anchors {
                                    left: lbl2.right
                                    leftMargin: 5
                                }

                                border.width: 1
                                color: Qt.rgba(Math.random() % 125, Math.random() % 125, Math.random() % 125)

                                MouseArea {
                                    anchors.fill: parent
                                    cursorShape: Qt.PointingHandCursor
                                    onClicked: {
                                        colorDialog.open();
                                    }
                                }

                                ColorDialog {
                                    id: colorDialog
                                    title: "Please choose a color"
                                    visible: false
                                    onAccepted: {
                                        colorRect.color = colorDialog.color;
                                    }
                                }
                            }
                        }

                        Item {
                            height: lineWidthSB.implicitHeight
                            width: parent.width

                            Label {
                                id: lbl3
                                anchors {
                                    verticalCenter: lineWidthSB.verticalCenter
                                    verticalCenterOffset: 2
                                    right: parent.left
                                    rightMargin: -70
                                }
                                text: "Line width:"
                            }
                            SpinBox {
                                id: lineWidthSB
                                width: 50

                                anchors {
                                    left: lbl3.right
                                    leftMargin: 5
                                }

                                value: 1
                                maximumValue: 5
                                minimumValue: 0
                                stepSize: 1
                                decimals: 0
                            }
                        }

                        Item {
                            height: multiplierSB.implicitHeight
                            width: parent.width

                            Label {
                                id: lbl4
                                anchors {
                                    verticalCenter: multiplierSB.verticalCenter
                                    verticalCenterOffset: 2
                                    right: parent.left
                                    rightMargin: -70
                                }
                                text: "Multiplier:"
                            }
                            SpinBox {
                                id: multiplierSB
                                width: 50

                                anchors {
                                    left: lbl4.right
                                    leftMargin: 5
                                }

                                value: 1.5
                                maximumValue: 5
                                minimumValue: 0
                                stepSize: 0.1
                                decimals: 1
                            }
                        }
                    }
                }
            }
        }
    }
}
