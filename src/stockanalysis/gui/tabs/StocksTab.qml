import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Styles 1.0
import QtQuick.Window 2.0
import QtGraphicalEffects 1.0
import "../"

Tab {
    id: tab
    title: "Stock"

    property var model

    function filterModel(model, role, pattern) {
        if (pattern.length > 0) {
            var array = new Array;
            model.forEach(function(s){
                if (s[role].indexOf(pattern) == 0) {
                    array.push(s);
                }
            });
            return array;
        }
        else {
            return model;
        }
    }

    function sortArray(array, role, ascending) {
        array.sort(function(a,b){
            var _a = a[role];
            var _b = b[role];
            if (ascending === Qt.AscendingOrder)
                return _a < _b ? -1 : (_a > _b ? 1 : 0);
            else
                return _a < _b ? 1 : (_a > _b ? -1 : 0);
        });
        return array;
    }

    Rectangle {
        focus: true
        TextField {
            id: filterField
            placeholderText: "Digite para filtrar"
            focus: false

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: 1
            }

            Rectangle {
                anchors {
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    rightMargin: 3
                }
                height: parent.height * 0.7
                width: height
                radius: height / 2
                color: "#CCCCCC"
                visible: filterField.text.length > 0
                Text { text: "X"; color: "white"; anchors.centerIn: parent }
                MouseArea {
                    anchors.fill: parent
                    onClicked: filterField.text = ""
                    cursorShape: Qt.PointingHandCursor
                }
            }
        }

        ScrollView {
            anchors {
                fill: parent
                topMargin: filterField.height
            }
            focus: true

            ListView {
                id: listView
                anchors.fill: parent
                property var filteredModel: sortArray(tab.model, "volume", false)
                model: filterModel(filteredModel, "code", filterField.text)
                currentIndex: 0
                focus: true
                activeFocusOnTab: true
                interactive: false

                Component.onCompleted: listView.forceActiveFocus()

                SystemPalette { id: syspal; colorGroup: listView.activeFocus ? SystemPalette.Active : SystemPalette.Inactive }

                Binding { target: root; property: "currentStock"; value: listView.model[listView.currentIndex]; }

                delegate: Rectangle {
                    id: row
                    property bool isCurrentItem: ListView.isCurrentItem
                    height: 20
                    width: parent.width
                    color: isCurrentItem ? syspal.highlight : (index % 2 ? syspal.alternateBase : syspal.base)
                    Label {
                        anchors {
                            verticalCenter: parent.verticalCenter
                            left: parent.left
                            leftMargin: 10
                        }
                        text: qsTr(modelData.code)
                        color: row.isCurrentItem ? syspal.highlightedText : syspal.windowText
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        var index = listView.indexAt(0, mouseY + listView.contentY);
                        listView.forceActiveFocus();
                        if (index > -1) {
                            listView.currentIndex = index;
                        }
                    }
                }

                Keys.onUpPressed: {
                    event.accepted = false
                    listView.decrementCurrentIndex()
                }

                Keys.onDownPressed: {
                    event.accepted = false
                    listView.incrementCurrentIndex()
                }
            }
        }
    }
}
