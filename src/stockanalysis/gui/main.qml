import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Styles 1.0
import QtQuick.Window 2.0
import QtGraphicalEffects 1.0
import "chart"
import "tabs"
import "analysis/ExponentialMovingAverage.js" as Lib

ApplicationWindow {
    id: root
    title: "StockAnalysis"

    width: 800
    height: 600
    minimumWidth: 80
    minimumHeight: 60

    property var currentStock

    Item {
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: rightColumn.left
            margins: 10
        }

        Label {
            id: stockNameLbl
            text: "<b>" + currentStock.name + "</b> - " + currentStock.code
            font.pixelSize: 24
        }

        VerticalSeparator {
            id: separator
            anchors {
                top: stockNameLbl.bottom
                left: parent.left
                right: parent.right
                topMargin: 5
            }
        }

        ColumnLayout {
            spacing: 5

            anchors {
                top: separator.bottom
                bottom: parent.bottom
                left: parent.left
                right: parent.right
                topMargin: 10
            }

            StockChart {
                id: stockChart
                title: "Diário"

                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.columnSpan: 2
                anchors {
                    left: parent.left
                    right: parent.right
                }

                model: root.currentStock.quotes

                topAxis {
                    ticks: 0
                    subTicks: 0
                }

                bottomAxis {
                    max: periodSelector.max
                    min: periodSelector.min
                    ticks: viewport.width / 100
                }

                leftAxis {
                    ticks: 0
                    subTicks: 0
                }
                rightAxis {
                    ticks: viewport.height / 40
                }

                grid {
                    verticalAxis: rightAxis
                    horizontalAxis: bottomAxis
                }
            }

            AccumulatedForceIndexChart {
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.maximumHeight: 150
                anchors {
                    left: parent.left
                    right: parent.right
                }

                model: stockChart.model
                topAxis {
                    ticks: 0
                    subTicks: 0
                }
                bottomAxis {
                    max: stockChart.mapper.horizontalAxis.max
                    min: stockChart.mapper.horizontalAxis.min
                    ticks: stockChart.bottomAxis.ticks
                    subTicks: stockChart.bottomAxis.subTicks
                }

                leftAxis {
                    ticks: 0
                    subTicks: 0
                }
                rightAxis {
                    ticks: viewport.height / 40
                }

                grid {
                    verticalAxis: rightAxis
                    horizontalAxis: bottomAxis
                }
            }

            PeriodSelector {
                id: periodSelector
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.maximumHeight: 40
                anchors {
                    left: parent.left
                    right: parent.right
                }

                model: stockChart.model
            }
        }
    }

    TabView {
        id: rightColumn
        width: 300
        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }

        StocksTab { model: presentation.stocks }
        MovingAverageTab { chart: stockChart }
        SafeZoneTab { chart: stockChart }
        EnvelopTab { chart: stockChart }
    }

    statusBar: Column {
        width: root.width
        height: childrenRect.height

        VerticalSeparator { width: parent.width }

        RowLayout {
            height: 20
            anchors {
                left: parent.left
                right: parent.right
                margins: 10
            }

            Label {
                text: "StockWizard, todos os direitos reservados a Wesley Ceraso Prudencio"
            }
            Item {
                Layout.fillHeight: true
                Layout.fillWidth: true
            }

        }
    }
}
