import QtQuick 2.1
import "../chart"
import "ExponentialMovingAverage.js" as Lib

QtObject {
    id: root

    signal paintRequest

    Component.onCompleted: {
        visibleChanged.connect(paintRequest);
        modelChanged.connect(paintRequest);
        periodChanged.connect(paintRequest);
        mapperChanged.connect(paintRequest);
        lineWidthChanged.connect(paintRequest);
        averageLineColorChanged.connect(paintRequest);
        parallelLinesColorChanged.connect(paintRequest);
        incrementChanged.connect(paintRequest);
    }

    property bool  visible: true
    property var   model: []
    property int   period: 15
    property int   increment: 10
    property ChartMapper mapper
    property real  lineWidth: 1.
    property color averageLineColor: "blue"
    property color parallelLinesColor: "green"

    property MovingAverage _averagePath: MovingAverage {
        mapper: root.mapper
        model: root.model
        period: root.period
        lineColor: averageLineColor
        lineWidth: root.lineWidth
        visible: root.visible
    }
    property ChartPath _upperPath: ChartPath {
        mapper: root.mapper
        model: Lib.shiftYPercentage(root._averagePath._averagePath.model, root.increment)
        lineColor: parallelLinesColor
        lineWidth: root.lineWidth
        visible: root.visible
    }
    property ChartPath _lowerPath: ChartPath {
        mapper: root.mapper
        model: Lib.shiftYPercentage(root._averagePath._averagePath.model, -root.increment)
        lineColor: parallelLinesColor
        lineWidth: root.lineWidth
        visible: root.visible
    }

    function onPaint(ctx) {
        _averagePath.onPaint(ctx);
        _upperPath.onPaint(ctx);
        _lowerPath.onPaint(ctx);
    }
}
