.pragma library

function movingAverage(values, period) {
    var av = new Array;
    if (values instanceof Array && values.length >= period && period > 0) {
        var aux = 0;

        for (var i=0; i<period; ++i) {
            aux += values[i];
        }
        av.push(aux/period);

        for (var i=period; i<values.length; ++i) {
            aux -= values[i-period];
            aux += values[i];
            av.push(aux/period);
        }
    }
    return av;
}

function exponentialMovingAverage(values, period) {
    var av = new Array;
    if (values instanceof Array && values.length >= period && period > 0) {
        av = movingAverage(values.slice(0, period), period);
        var ma = av[0];
        for (var i=period; i<values.length; ++i) {
            var k = 2 / (period+1);
            ma = values[i] * k + ma * (1-k);
            av.push(ma);
        }
    }
    return av;
}

function pointsExponentialMovingAverage(quotes, period)
{
    var input = new Array;
    quotes.forEach(function(q){
        input.push(q.y);
    });
    return exponentialMovingAverage(array, period);
}

function quotesMovingAverage(quotes, period)
{
    var input = new Array;
    quotes.forEach(function(q){
        input.push(q.close);
    });
    return movingAverage(input, period);
}

function quotesExponentialMovingAverage(quotes, period)
{
    var input = new Array;
    quotes.forEach(function(q){
        input.push(q.close);
    });
    return exponentialMovingAverage(input, period);
}

function makePointsFrom(values, offsetX) {
    var output = new Array;
    values.forEach(function(v){
        output.push(Qt.point(offsetX++, v));
    });
    return output;
}

function shiftYPercentage(points, increment) {
    var ar = new Array;
    points.forEach(function(p){
        ar.push(Qt.point(p.x, p.y * (1+increment/100)))
    });
    return ar;
}

function safeZone(quotes, period, multiplier) {
    var sZp = new Array;
    if (quotes.length > period && period > 0) {
        for (var i=period-1; i<quotes.length; ++i) {
            var fallCounter = 0;
            var fallAverage = 0;
            for (var j=i-period+2; j<=i; ++j) {
                var diff = quotes[j-1].low - quotes[j].low;
                if (diff > 0) {
                    ++fallCounter;
                    fallAverage += diff;
                }
            }
            if (fallCounter) fallAverage /= fallCounter;
            sZp.push(Qt.point(i, quotes[i].low - (multiplier * fallAverage)));
        }
    }
    return sZp;
}

function macd(quotes, period1, period2, period3) {
    var period1EMA = quotesExponentialMovingAverage(quotes, period1);
    var period2EMA = quotesExponentialMovingAverage(quotes, period2);
    var diff = period2 - period1;

    var fastLine = new Array;
    for (var i=0; i<period2EMA.length; ++i) {
        fastLine.push(period1EMA[i+diff]-period2EMA[i]);
    }

    var slowLine = exponentialMovingAverage(fastLine, period3);

    var histogram = new Array;
    for (var i=0; i<slowLine.length; ++i) {
        histogram.push(fastLine[i+period3-1]-slowLine[i]);
    }

    fastLine = makePointsFrom(fastLine, period2-1);
    slowLine = makePointsFrom(slowLine, period2+period3-2)
    histogram = makePointsFrom(histogram, period2+period3-2)

    return [fastLine, slowLine, histogram];
}

function forceIndex(quotes, period1) {
    var fIndex = new Array;
    for (var i=1; i<quotes.length; ++i) {
        fIndex.push((quotes[i].close - quotes[i-1].close)*quotes[i].volume);
    };

    return makePointsFrom(exponentialMovingAverage(fIndex, period1), period1);
}

function accumulatedForceIndex(quotes) {
    var fIndex = new Array;
    var value = 0.;
    for (var i=1; i<quotes.length; ++i) {
        value += (quotes[i].close - quotes[i-1].close)*quotes[i].volume;
        fIndex.push(value);
    };

    return makePointsFrom(fIndex, 1);
}

function tradingVolume(quotes) {
    var output = new Array;
    quotes.forEach(function(q){
        output.push(q.volume);
    });

    return makePointsFrom(output, 0);
}

function findMaxMin(model, topBorder, bottomBorder) {
    var r = new Array;
    if (model.length) {
        var start = 0;
        var _yMax = model[start];
        var _yMin = model[start];
        for (var i=start; i<model.length; ++i) {
            _yMax = Math.max(_yMax, model[i]);
            _yMin = Math.min(_yMin, model[i]);
        }
        var upperBorder = (_yMax - _yMin) * topBorder / 100;
        var lowerBorder = (_yMax - _yMin) * bottomBorder / 100;
        _yMax += upperBorder;
        _yMin -= lowerBorder;

        r.push(_yMax);
        r.push(_yMin);
    }
    return r;
}

function findMaxMinQuotes(model, topBorder, bottomBorder) {
    var input = new Array;
    if (model.length) {
        for (var i=0; i<model.length; ++i) {
            input.push(model[i].low);
            input.push(model[i].high);
        }
    }
    return findMaxMin(input, topBorder, bottomBorder);
}

function findMaxMinPoints(model, topBorder, bottomBorder) {
    var input = new Array;
    if (model.length) {
        for (var i=0; i<model.length; ++i) {
            input.push(model[i].y);
        }
    }
    return findMaxMin(input, topBorder, bottomBorder);
}
