import QtQuick 2.1
import "../chart"
import "ExponentialMovingAverage.js" as Lib

QtObject {
    id: root

    signal paintRequest

    Component.onCompleted: {
        visibleChanged.connect(paintRequest);
        modelChanged.connect(paintRequest);
        periodChanged.connect(paintRequest);
        mapperChanged.connect(paintRequest);
        lineWidthChanged.connect(paintRequest);
        lineColorChanged.connect(paintRequest);
    }

    property bool  visible: true
    property var   model: []
    property int   period: 15
    property ChartMapper mapper
    property real  lineWidth: 1.
    property color lineColor: "blue"

    property ChartPath _averagePath: ChartPath {
        mapper: root.mapper
        model: Lib.makePointsFrom(Lib.quotesMovingAverage(root.model, root.period), root.period - 1)
        lineColor: root.lineColor
        lineWidth: root.lineWidth
        visible: root.visible
    }

    function onPaint(ctx) {
        _averagePath.onPaint(ctx);
    }
}
