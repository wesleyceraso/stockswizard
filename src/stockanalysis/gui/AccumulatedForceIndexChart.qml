import QtQuick 2.1
import "chart"
import "analysis"
import "analysis/ExponentialMovingAverage.js" as Lib

ChartXY {
    id: root

    property var model: []
    property color lineColor: "red"
    property real lineWidth: 1

    property real yMax
    property real yMin

    onModelChanged: root.requestPaint();

    property ChartMapper mapper: ChartMapper {
        id: chartMapper
        horizontalAxis: root.bottomAxis
        verticalAxis: root.rightAxis
        viewport: root.viewport
    }

    QtObject {
        id: _internal
        property var line: Lib.accumulatedForceIndex(root.model);
        property var max_min: Lib.findMaxMinPoints(line.slice(
                                                       Math.min(Math.max(mapper.horizontalAxis.min, 0), root.model.length),
                                                       Math.min(Math.max(mapper.horizontalAxis.max, 0), root.model.length)), 5, 5);
    }

    rightAxis {
        min: _internal.max_min[0]
        max: _internal.max_min[1]
    }

    drawableChildren: [
        ChartPath {
            id: macdFast
            model: _internal.line
            mapper: root.mapper
            lineColor: root.lineColor
            lineWidth: root.lineWidth
        },
        ChartLabel {
            labels: {
                var r = new Array;
                var p1 = new Object;
                p1.color = root.lineColor;
                p1.label = "Accumulated Force Index";
                r.push(p1);
                return r;
            }

            mapper: root.mapper
        }
    ]
}
