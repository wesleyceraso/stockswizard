#ifndef STOCKANALYSISWINDOW_H
#define STOCKANALYSISWINDOW_H

#include <QObject>
#include <QVariant>
#include <QDate>
#include "../../stockexchange/bmfstockexchange.h"

class StockAnalysisWindow : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList stocks READ stocks NOTIFY stocksChanged)
    Q_PROPERTY(QDate upToDate READ upToDate NOTIFY stocksChanged)
public:
    explicit StockAnalysisWindow(QObject *parent = 0);

public slots:
    void load();

    QVariantList stocks() const;
    QDate upToDate() const;

    QVariantMap getEnvelop(QString stockCode, int period) const;

signals:
    void stocksChanged();

private:
    BmfStockExchange m_bmf;
    QVariantList m_stocks;
    QDate m_upToDate;
};

#endif // STOCKANALYSISWINDOW_H
