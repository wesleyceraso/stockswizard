import QtQuick 2.0

QtObject {
    id: root

    signal paintRequest

    Component.onCompleted: {
        minChanged.connect(paintRequest);
        maxChanged.connect(paintRequest);
        ticksChanged.connect(paintRequest);
        tickSizeChanged.connect(paintRequest);
        subTicksChanged.connect(paintRequest);
        subTickSizeChanged.connect(paintRequest);
        lineWidthChanged.connect(paintRequest);
        lineColorChanged.connect(paintRequest);
    }

    property real min
    property real max
    readonly property real length: max - min
    property int ticks: 4
    property int tickSize: 5
    property int subTicks: 4
    property int subTickSize: 3
    property real lineWidth: 1
    property color lineColor: "black"
}
