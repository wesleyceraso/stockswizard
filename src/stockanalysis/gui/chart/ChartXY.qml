import QtQuick 2.0

Rectangle {
    id: root

    property list<QtObject> drawableChildren
    property var dynamicDrawableChildren: []
    property rect viewport: Qt.rect(0, 0, viewport.width, viewport.height)
    property color background: "white"

    property ChartAxis leftAxis: ChartAxis{}
    property ChartAxis rightAxis: ChartAxis{}
    property ChartAxis topAxis: ChartAxis{}
    property ChartAxis bottomAxis: ChartAxis{}
    property ChartAxis horizontalCenterAxis: ChartAxis{}
    property ChartAxis verticalCenterAxis: ChartAxis{}
    property ChartGrid grid: ChartGrid{ horizontalAxis: bottomAxis; verticalAxis: leftAxis; }

    property alias interactive: mouseArea.enabled

    border {
        width: 1
        color: "#808080"
    }

    function requestPaint() {
        outterCanvas.requestPaint();
        innerCanvas.requestPaint();
    }

    function addItem(item) {
        dynamicDrawableChildren.push(item);
        item.paintRequest.connect(requestPaint);
        requestPaint();
    }

    function removeItem(item) {
        dynamicDrawableChildren.splice(dynamicDrawableChildren.indexOf(item), 1);
        requestPaint();
    }

    Canvas {
        id: outterCanvas
        contextType: "2d"
        anchors {
            fill: parent
            margins: 1
        }
        antialiasing: false

        Canvas {
            id: innerCanvas
            contextType: "2d"
            antialiasing: true//false

            anchors {
                fill: parent
            }

            Item { id: viewport; anchors.fill: parent }

            onPaint: {
                var ctx = innerCanvas.context;
                if (ctx === null) return;
                ctx.clearRect(0, 0, innerCanvas.width, innerCanvas.height);

                for (var i=0; i<drawableChildren.length; ++i) {
                    drawableChildren[i].onPaint(ctx)
                }

                dynamicDrawableChildren.forEach(function(c){
                    c.onPaint(ctx);
                });
            }
        }

        MouseArea {
            id: mouseArea
            anchors.fill: innerCanvas
            hoverEnabled: true
            onPositionChanged: outterCanvas.requestPaint();
            onExited: outterCanvas.requestPaint();
        }

        onPaint: {
            var ctx = outterCanvas.context;
            if (ctx === null) return;
            ctx.clearRect(0, 0, outterCanvas.width, outterCanvas.height);

            var rect = Qt.rect(innerCanvas.anchors.leftMargin, innerCanvas.anchors.topMargin,
                               innerCanvas.width, innerCanvas.height);

            ctx.fillStyle = root.background;
            ctx.fillRect(rect.x, rect.y, rect.width, rect.height);
            ctx.fill();

            var leftTop = Qt.point(rect.x, rect.y);
            var leftBottom = Qt.point(rect.x, rect.y + rect.height);
            var rightTop = Qt.point(rect.x + rect.width, rect.y);
            var rightBottom = Qt.point(rect.x + rect.width, rect.y + rect.height);

            grid.onPaint(leftTop, rightBottom, ctx);

            if (mouseArea.containsMouse) {
                var mousePoint = Qt.point(mouseArea.mouseX, mouseArea.mouseY);
                var inScaleMousePoint = mapper.mapFrom(mousePoint)[0];
                var point = Qt.point(Math.floor(mousePoint.x + leftTop.x), Math.floor(mousePoint.y + leftTop.y));

                ctx.strokeStyle = "grey";
                ctx.lineWidth = 1.;

                ctx.beginPath();
                ctx.moveTo(Math.floor(leftTop.x), point.y);
                ctx.lineTo(Math.floor(rightTop.x), point.y);
                ctx.moveTo(point.x, Math.floor(leftTop.y));
                ctx.lineTo(point.x, Math.floor(leftBottom.y));
                ctx.closePath();
                ctx.stroke();

                ctx.textBaseline = "alphabetic";
                ctx.textAlign = "start";
                ctx.fillStyle = "black";
                ctx.fillText("[" + inScaleMousePoint.x.toFixed(2) + "," + inScaleMousePoint.y.toFixed(2) + "]", point.x + 5, point.y - 5);
                ctx.fill();
            }
        }
    }

    Connections { target: leftAxis; onPaintRequest: root.requestPaint(); }
    Connections { target: rightAxis; onPaintRequest: root.requestPaint(); }
    Connections { target: topAxis; onPaintRequest: root.requestPaint(); }
    Connections { target: bottomAxis; onPaintRequest: root.requestPaint(); }
    Connections { target: verticalCenterAxis; onPaintRequest: root.requestPaint(); }
    Connections { target: horizontalCenterAxis; onPaintRequest: root.requestPaint(); }
}
