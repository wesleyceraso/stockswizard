import QtQuick 2.0

QtObject {
    id: root

    property bool visible: true
    property ChartAxis horizontalAxis
    property ChartAxis verticalAxis
    property real lineWidth: 1
    property color lineColor: "darkgray"

    function onPaint(topLeft, rightBottom, ctx) {
        if (visible) {
            ctx.save();
            ctx.lineWidth = root.lineWidth;
            ctx.strokeStyle = root.lineColor;
            ctx.beginPath();

            if (horizontalAxis != null) {
                var deltaX = (rightBottom.x - topLeft.x) / (horizontalAxis.ticks + 1);
                for (var i=1; i<horizontalAxis.ticks + 1; ++i) {
                    var x = topLeft.x + deltaX * i;
                    ctx.moveTo(x, topLeft.y);
                    ctx.lineTo(x, rightBottom.y);
                }
            }

            if (verticalAxis != null) {
                var deltaY = (rightBottom.y - topLeft.y) / (verticalAxis.ticks + 1);
                for (var i=1; i<verticalAxis.ticks + 1; ++i) {
                    var y = topLeft.y + deltaY * i;
                    ctx.moveTo(topLeft.x, y);
                    ctx.lineTo(rightBottom.x, y);
                }
            }

            ctx.closePath();
            ctx.stroke();
            ctx.restore();
        }
    }
}
