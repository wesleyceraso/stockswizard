import QtQuick 2.0

QtObject {
    id: root

    property bool visible: true
    property var  model: []
    property ChartMapper mapper
    property real  lineWidth: 1.
    property color lineColor: "black"

    property bool  drawPoints: false
    property int   pointSize: 3;

    function onPaint(ctx) {
        if (visible) {
            ctx.lineWidth = root.lineWidth;
            ctx.strokeStyle = lineColor;
            ctx.beginPath();

            var mappedPoints = mapper.mapTo(model);

            var start = mappedPoints[0];
            ctx.moveTo(mappedPoints.x, mappedPoints.y);
            mappedPoints.forEach(function(p){
                ctx.lineTo(p.x, p.y);
                if (drawPoints) {
                    var rect = Qt.rect(p.x - pointSize, p.y - pointSize, pointSize * 2, pointSize * 2);
                    ctx.rect(rect.x, rect.y, rect.width, rect.height);
                }
                ctx.moveTo(p.x, p.y);
            });

            ctx.closePath();
            ctx.stroke();
        }
    }
}
