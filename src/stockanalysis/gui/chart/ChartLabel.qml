import QtQuick 2.0

QtObject {
    id: root

    property bool visible: true
    property var labels: []
    property int spacing: 12
    property ChartMapper mapper

    function onPaint(ctx) {
        if (visible) {
            ctx.save();

            ctx.textBaseline = "middle"

            for (var i=0; i<labels.length; ++i) {
                ctx.beginPath();
                ctx.fillStyle = labels[i]["color"];
                ctx.fillRect(mapper.viewport.x + 10, mapper.viewport.y + (i + 1) * root.spacing - 4, 8, 8);
                ctx.closePath();
                ctx.fill();

                ctx.beginPath();
                ctx.fillStyle = "black";
                ctx.fillText(labels[i]["label"], mapper.viewport.x + 20, mapper.viewport.y + (i + 1) * root.spacing);
                ctx.closePath();
                ctx.fill();
            }
        }
    }
}
