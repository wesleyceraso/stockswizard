import QtQuick 2.0

QtObject {
    property rect viewport
    property ChartAxis horizontalAxis
    property ChartAxis verticalAxis
//    property matrix4x4 tranformTo: Qt.
//    property matrix4x4 tranformFrom: tranformTo.inverted();

    function mapTo(points) {
        if (!(points instanceof Array))
            points = [ points ];

        var result = new Array;
        points.forEach(function(p) {
            result.push(Qt.point(
                viewport.x + viewport.width * (p.x - horizontalAxis.min) / horizontalAxis.length,
                viewport.y + viewport.height * (p.y - verticalAxis.min) / verticalAxis.length
            ));
        });
        return result;
    }

    function mapFrom(points) {
        if (!(points instanceof Array))
            points = [ points ];

        var result = new Array;
        points.forEach(function(p) {
            result.push(Qt.point(
                horizontalAxis.min + horizontalAxis.length * (p.x - viewport.x) / viewport.width,
                verticalAxis.min + verticalAxis.length * (p.y - viewport.y) / viewport.height
            ));
        });
        return result;
    }
}
