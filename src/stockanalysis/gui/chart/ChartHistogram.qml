import QtQuick 2.0

QtObject {
    id: root

    property bool visible: true
    property var model: []
    property ChartMapper mapper
    property int  blockWidth: 2.
    property color blockColor: "green"

    function onPaint(ctx) {
        if (visible) {
            ctx.strokeStyle = Qt.darker(root.blockColor);
            ctx.lineWidth = 1.;
            ctx.fillStyle = root.blockColor;

            ctx.beginPath();
            var pointZero = mapper.mapTo(Qt.point(0, 0))[0];
            var mappedPoints = mapper.mapTo(root.model);

            mappedPoints.forEach(function(p){
               ctx.rect(p.x - root.blockWidth, pointZero.y, root.blockWidth*2, p.y - pointZero.y);
            });
            ctx.closePath();

            ctx.fill();
            ctx.stroke();
        }
    }
}
