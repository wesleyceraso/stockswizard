import QtQuick 2.0

QtObject {
    id: root

    property bool visible: true
    property var candles: []
    property ChartMapper mapper
    property real  lineWidth: 1.
    property color upColor: "green"
    property color downColor: "red"

    function onPaint(ctx) {
        if (visible) {
            ctx.save();
            ctx.lineWidth = root.lineWidth;
            ctx.strokeStyle = "black";

            for (var i=0; i<candles.length; ++i)
            {
                ctx.beginPath();

                var c = candles[i];
                var points = new Array;
                points.push(Qt.point(i, c.high))
                points.push(Qt.point(i, c.low));
                points.push(Qt.point(i, c.open))
                points.push(Qt.point(i, c.close));
                var mappedPoints = mapper.mapTo(points);

                if (c.close>c.open) {
                    ctx.fillStyle = upColor;
                }
                else {
                    ctx.fillStyle = downColor;
                }

                ctx.moveTo(mappedPoints[0].x, mappedPoints[0].y);
                ctx.lineTo(mappedPoints[1].x, mappedPoints[1].y);

                ctx.rect(mappedPoints[2].x - 3, mappedPoints[2].y, 6, mappedPoints[3].y - mappedPoints[2].y);

                ctx.closePath();
                ctx.stroke();
                ctx.fill();
            }
            ctx.restore();
        }
    }
}
