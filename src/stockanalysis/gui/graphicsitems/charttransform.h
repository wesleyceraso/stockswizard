#ifndef CHARTTRANSFORM_H
#define CHARTTRANSFORM_H

#include <QSharedPointer>
#include <QPointF>
#include <QVector>
#include <QList>
#include "scale.h"

class ChartTransform
{
public:
    explicit ChartTransform( const QSharedPointer<Scale> & scaleX, const QSharedPointer<Scale> & scaleY );

    QSharedPointer<Scale> scaleX() const;
    void setScaleX(const QSharedPointer<Scale> & scaleX);

    QSharedPointer<Scale> scaleY() const;
    void setScaleY(const QSharedPointer<Scale> & scaleY);

    QPointF          mapTo( const QPointF & ) const;
    QList<QPointF>   mapTo( const QList<QPointF> & ) const;
    QVector<QPointF> mapTo( const QVector<QPointF> & ) const;

    QPointF          mapFrom( const QPointF & ) const;
    QList<QPointF>   mapFrom( const QList<QPointF> & ) const;
    QVector<QPointF> mapFrom( const QVector<QPointF> & ) const;

private:
    QSharedPointer<Scale> m_scaleX;
    QSharedPointer<Scale> m_scaleY;
};

#endif // CHARTTRANSFORM_H
