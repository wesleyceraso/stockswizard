#ifndef GRAPHICCANDLE_H
#define GRAPHICCANDLE_H

#include "chartitem.h"

class Candle {
public:
    double x;
    double min;
    double max;
    double open;
    double close;
};

class GraphicCandle : public ChartItem
{
public:
    explicit GraphicCandle( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent = 0 );
    explicit GraphicCandle( const QList< Candle > & candles, QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent = 0 );
    explicit GraphicCandle( const QVector< Candle > & candles, QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent = 0 );

    void setCandles( const QList< Candle > & candles );
    void setCandles( const QVector< Candle > & candles );

    QVector< Candle > toVector() const { return m_candles; }
    QList< Candle > toList() const { return m_candles.toList(); }

    virtual void   paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget );

private:
    QVector< Candle > m_candles;

};

#endif // GRAPHICCANDLE_H
