#include "graphiccandle.h"
#include <QPainter>

GraphicCandle::GraphicCandle( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent ):
    ChartItem( scaleX, scaleY, parent )
{}

GraphicCandle::GraphicCandle( const QList< Candle > & candles, QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent ):
    ChartItem( scaleX, scaleY, parent ),
    m_candles( candles.toVector() )
{
}

GraphicCandle::GraphicCandle( const QVector< Candle > & candles, QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent ):
    ChartItem( scaleX, scaleY, parent ),
    m_candles( candles )
{
}

void GraphicCandle::setCandles(const QList<Candle> &candles)
{
    m_candles = candles.toVector();
}

void GraphicCandle::setCandles(const QVector<Candle> &candles)
{
    m_candles = candles;
}

void GraphicCandle::paint( QPainter * painter, const QStyleOptionGraphicsItem *, QWidget * )
{
    if ( m_candles.isEmpty() ) return;

    painter->save();
    painter->setClipRect( boundingRect() );

    foreach ( const Candle & c, m_candles )
    {
        painter->setPen( Qt::black );
        QPointF min( c.x, c.min );
        QPointF max( c.x, c.max );
        painter->drawLine( transform()->mapTo( min ),
                           transform()->mapTo( max ) );

        painter->setBrush( c.open < c.close ? Qt::white : Qt::black );
        QPointF origin = transform()->mapTo( QPointF( c.x, qMin( c.open, c.close ) ) );
        origin.setX( origin.x() - 5 );
        QPointF extended = transform()->mapTo( QPointF( c.x, qMax( c.open, c.close ) ) );
        painter->drawRect( QRectF( origin, QSizeF( 10, extended.y() - origin.y() ) ) );
    }
    painter->restore();
}
