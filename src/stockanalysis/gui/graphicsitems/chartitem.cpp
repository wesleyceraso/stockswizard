#include "chartitem.h"

ChartItem::ChartItem( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent ):
    QGraphicsItem( parent ),
    m_scaleX( scaleX ),
    m_scaleY( scaleY ),
    m_transform( new ChartTransform( scaleX, scaleY ) )
{
}

QRectF ChartItem::rect() const
{
    return m_rect;
}

void ChartItem::setRect( const QRectF & rect )
{
    m_rect = rect;
}

void ChartItem::setRect( qreal x, qreal y, qreal width, qreal height )
{
    setRect( QRectF( x, y, width, height ) );
}

QSharedPointer<Scale> ChartItem::scaleY() const
{
    return m_scaleY;
}

void ChartItem::setScaleY( const QSharedPointer<Scale> & scaleY )
{
    m_scaleY = scaleY;
    m_transform->setScaleY( m_scaleY );
}

QSharedPointer<Scale> ChartItem::scaleX() const
{
    return m_scaleX;
}

void ChartItem::setScaleX( const QSharedPointer<Scale> & scaleX )
{
    m_scaleX = scaleX;
    m_transform->setScaleX( m_scaleX );
}

QSharedPointer<ChartTransform> ChartItem::transform() const
{
    return m_transform;
}

QRectF ChartItem::boundingRect() const
{
    Range<qreal> rangeX = m_scaleX->targetRange();
    Range<qreal> rangeY = m_scaleY->targetRange();
    return QRectF( rangeX.min(), rangeY.min(), rangeX.length(), rangeY.length() );
}
