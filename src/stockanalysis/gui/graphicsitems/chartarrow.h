#ifndef CHARTARROW_H
#define CHARTARROW_H

#include "chartitem.h"
#include <QPointF>
#include <QBrush>
#include <QPen>

class ChartArrow : public ChartItem
{
public:
    explicit ChartArrow( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent = 0 );
    explicit ChartArrow( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, QPointF origin, qreal angle = 0., ChartItem * parent = 0 );

    QPointF origin() const;
    void setOrigin(const QPointF & origin);

    qreal angle() const;
    void setAngle(const qreal & angle);

    QBrush brush() const;
    void setBrush(const QBrush & brush);

    QPen pen() const;
    void setPen(const QPen & pen);

    virtual void   paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget );

private:
    void initializePolygon();

private:
    QPointF m_origin;
    qreal m_angle;
    QBrush m_brush;
    QPen m_pen;
    QPolygonF m_polygon;
};

#endif // CHARTARROW_H
