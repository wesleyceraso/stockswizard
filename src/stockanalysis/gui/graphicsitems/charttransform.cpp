#include "charttransform.h"

ChartTransform::ChartTransform( const QSharedPointer<Scale> & scaleX, const QSharedPointer<Scale> & scaleY ):
    m_scaleX( scaleX ),
    m_scaleY( scaleY )
{
}

QSharedPointer<Scale> ChartTransform::scaleX() const
{
    return m_scaleX;
}

void ChartTransform::setScaleX( const QSharedPointer<Scale> & scaleX )
{
    m_scaleX = scaleX;
}

QSharedPointer<Scale> ChartTransform::scaleY() const
{
    return m_scaleY;
}

void ChartTransform::setScaleY( const QSharedPointer<Scale> & scaleY )
{
    m_scaleY = scaleY;
}

QPointF ChartTransform::mapTo( const QPointF & point ) const
{
    return QPointF( m_scaleX->mapTo( point.x() ), m_scaleY->mapTo( point.y() ) );
}

QList<QPointF> ChartTransform::mapTo( const QList<QPointF> & points ) const
{
    QList<QPointF> result;
    for ( const QPointF & p: points )
    {
        result.append( mapTo( p ) );
    }
    return result;
}

QVector<QPointF> ChartTransform::mapTo( const QVector<QPointF> & points ) const
{
    QVector<QPointF> result;
    for ( const QPointF & p: points )
    {
        result.append( mapTo( p ) );
    }
    return result;
}

QPointF ChartTransform::mapFrom( const QPointF & point ) const
{
    return QPointF( m_scaleX->mapFrom( point.x() ), m_scaleY->mapFrom( point.y() ) );
}

QList<QPointF> ChartTransform::mapFrom( const QList<QPointF> & points ) const
{
    QList<QPointF> result;
    for ( const QPointF & p: points )
    {
        result.append( mapFrom( p ) );
    }
    return result;
}

QVector<QPointF> ChartTransform::mapFrom( const QVector<QPointF> & points ) const
{
    QVector<QPointF> result;
    for ( const QPointF & p: points )
    {
        result.append( mapFrom( p ) );
    }
    return result;
}
