#ifndef GRAPHICFRAME_H
#define GRAPHICFRAME_H

#include "chartitem.h"
#include <QSharedPointer>
#include "scale.h"

class ChartFrame : public QGraphicsItem
{
public:
    explicit ChartFrame( QGraphicsItem * parent = 0 );

    QRectF rect() const;
    void setRect( const QRectF & rect );
    void setRect( qreal x, qreal y, qreal width, qreal height );

    void setLeftScale  ( QSharedPointer<Scale> );
    void setRightScale ( QSharedPointer<Scale> );
    void setTopScale   ( QSharedPointer<Scale> );
    void setBottomScale( QSharedPointer<Scale> );
    void setVerticalCenterScale( QSharedPointer<Scale> );
    void setHorizontalCenterScale( QSharedPointer<Scale> );

    QSharedPointer<Scale> leftScale  () const { return m_leftScale; }
    QSharedPointer<Scale> rightScale () const { return m_rightScale; }
    QSharedPointer<Scale> topScale   () const { return m_topScale; }
    QSharedPointer<Scale> bottomScale() const { return m_bottomScale; }
    QSharedPointer<Scale> verticalCenterScale  () const { return m_verticalCenterScale; }
    QSharedPointer<Scale> horizontalCenterScale() const { return m_horizontalCenterScale; }

    virtual QRectF boundingRect() const { return m_rect; }
    virtual void   paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget );

private:
    void drawBackBone( QPainter * painter, const QSharedPointer<Scale> & scale, Qt::Orientation orientation, bool inverted = false ) const;

private:
    QRectF m_rect;
    QSharedPointer<Scale> m_leftScale;
    QSharedPointer<Scale> m_rightScale;
    QSharedPointer<Scale> m_topScale;
    QSharedPointer<Scale> m_bottomScale;
    QSharedPointer<Scale> m_verticalCenterScale;
    QSharedPointer<Scale> m_horizontalCenterScale;
};

#endif // GRAPHICFRAME_H
