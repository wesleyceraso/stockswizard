#include "chartlinearscalestrategy.h"

ChartLinearScaleStrategy::ChartLinearScaleStrategy():
    ChartScaleStrategy()
{
}

qreal ChartLinearScaleStrategy::mapTo( qreal value, const Range<qreal> & originRange, const Range<qreal> & targetRange ) const
{
    return targetRange.min() + targetRange.difference() * ( ( value - originRange.min() ) / originRange.difference() );
}
