#ifndef CHARTSCALESTRATEGY_H
#define CHARTSCALESTRATEGY_H

#include <range.h>
#include <QVector>
#include <QList>

class ChartScaleStrategy
{
public:
    ChartScaleStrategy();
    virtual ~ChartScaleStrategy() {}

    virtual qreal  mapTo( qreal value, const Range<qreal> & originRange, const Range<qreal> & targetRange ) const = 0;
    QList<qreal>   mapTo( const QList<qreal> & values, const Range<qreal> & originRange, const Range<qreal> & targetRange ) const;
    QVector<qreal> mapTo( const QVector<qreal> & values, const Range<qreal> & originRange, const Range<qreal> & targetRange ) const;
};

#endif // CHARTSCALESTRATEGY_H
