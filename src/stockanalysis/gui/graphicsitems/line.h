#ifndef GRAPHICCURVE_H
#define GRAPHICCURVE_H

#include "chartitem.h"
#include <QPen>

class Line : public ChartItem
{
public:
    explicit Line( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent = 0 );
    explicit Line( QVector< QPointF >, QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent = 0 );
    explicit Line( QList< QPointF >, QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent = 0 );

    void setPoints( const QVector< QPointF > & );
    void setPoints( const QList< QPointF > & );

    QVector< QPointF > toVector() const { return m_points; }
    QList< QPointF > toList() const { return m_points.toList(); }

    QPen pen() const;
    void setPen(const QPen & pen);

    virtual void   paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget );

private:
    QPainterPath m_path;
    QVector< QPointF > m_points;
    QPen m_pen;
};

#endif // GRAPHICCURVE_H
