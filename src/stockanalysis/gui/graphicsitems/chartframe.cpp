#include "chartframe.h"
#include <QPainter>
#include <QRectF>
#include <QTextOption>

ChartFrame::ChartFrame( QGraphicsItem * parent ):
    QGraphicsItem( parent ),
    m_leftScale( new Scale ),
    m_rightScale( new Scale ),
    m_topScale( new Scale ),
    m_bottomScale( new Scale ),
    m_verticalCenterScale( new Scale ),
    m_horizontalCenterScale( new Scale )
{
    m_verticalCenterScale->setVisible( false );
    m_horizontalCenterScale->setVisible( false );
}

QRectF ChartFrame::rect() const
{
    return m_rect;
}

void ChartFrame::setRect( const QRectF & rect )
{
    m_rect = rect;
    m_leftScale->setTargetRange( rect.top(), rect.bottom() );
    m_rightScale->setTargetRange( rect.top(), rect.bottom() );
    m_verticalCenterScale->setTargetRange( rect.top(), rect.bottom() );
    m_topScale->setTargetRange( rect.left(), rect.right() );
    m_bottomScale->setTargetRange( rect.left(), rect.right() );
    m_horizontalCenterScale->setTargetRange( rect.left(), rect.right() );
}

void ChartFrame::setRect(qreal x, qreal y, qreal width, qreal height)
{
    setRect( QRectF( x, y, width, height ) );
}

void ChartFrame::setLeftScale( QSharedPointer<Scale> scale )
{
    m_leftScale = scale;
}

void ChartFrame::setRightScale( QSharedPointer<Scale> scale )
{
    m_rightScale = scale;
}

void ChartFrame::setTopScale( QSharedPointer<Scale> scale )
{
    m_topScale = scale;
}

void ChartFrame::setBottomScale( QSharedPointer<Scale> scale )
{
    m_bottomScale = scale;
}

void ChartFrame::setVerticalCenterScale( QSharedPointer<Scale> scale )
{
    m_verticalCenterScale = scale;
}

void ChartFrame::setHorizontalCenterScale( QSharedPointer<Scale> scale )
{
    m_horizontalCenterScale = scale;
}

void ChartFrame::paint( QPainter * painter, const QStyleOptionGraphicsItem *, QWidget * )
{
    painter->save();

    //m_topScale
    painter->translate( m_rect.topLeft() );
    drawBackBone( painter, m_topScale, Qt::Horizontal, true );

    painter->resetTransform();

    //m_bottomScale
    painter->translate( m_rect.bottomLeft() );
    drawBackBone( painter, m_bottomScale, Qt::Horizontal );

    painter->resetTransform();

    //m_horizontalCenterScale
    painter->translate( QPointF( m_rect.left(), m_rect.top() + m_rect.height() / 2 ) );
    drawBackBone( painter, m_horizontalCenterScale, Qt::Horizontal );

    painter->resetTransform();

    //m_leftScale
    painter->translate( m_rect.topLeft() );
    drawBackBone( painter, m_leftScale, Qt::Vertical );

    painter->resetTransform();

    //m_rightScale
    painter->translate( m_rect.topRight() );
    drawBackBone( painter, m_rightScale, Qt::Vertical, true );

    painter->resetTransform();

    //m_verticalCenterScale
    painter->translate( QPointF( m_rect.left() + m_rect.width() / 2, m_rect.top() ) );
    drawBackBone( painter, m_verticalCenterScale, Qt::Vertical );

    painter->restore();
}

void ChartFrame::drawBackBone( QPainter * painter, const QSharedPointer<Scale> & scale, Qt::Orientation orientation, bool inverted ) const
{
    if ( scale->isVisible() )
    {
        qreal length = scale->targetRange().length();

        if ( orientation == Qt::Vertical )
            painter->rotate( 90 );

        painter->drawLine( 0, 0, length, 0 );

        if ( !scale->ticks() )
            return;

        unsigned int totalTicks = scale->ticks() + 2;
        qreal targetDelta = scale->targetRange().length() / totalTicks;

        qreal tickSize = scale->tickSize();
        qreal subTickSize = scale->subTickSize();

        Scale::TickPosition tickPos = scale->tickPosition();
        Scale::TickPosition subTickPos = scale->subTickPosition();

        qreal y1 = tickPos == Scale::IN_OUT || tickPos == Scale::IN ? -tickSize : 0;
        qreal y2 = tickPos == Scale::IN_OUT || tickPos == Scale::OUT ? tickSize : 0;

        qreal yy1 = subTickPos == Scale::IN_OUT || subTickPos == Scale::IN ? -subTickSize : 0;
        qreal yy2 = subTickPos == Scale::IN_OUT || subTickPos == Scale::OUT ? subTickSize : 0;

        if ( inverted )
        {
            y1 = -y1;
            y2 = -y2;
            yy1 = -yy1;
            yy2 = -yy2;
        }

        for ( unsigned int i = 0; i <= totalTicks; ++i )
        {
            qreal x = i * targetDelta;
            painter->drawLine( QPointF( x, y1 ), QPointF( x, y2 ) );

            qreal subDelta = ( scale->mapFrom( scale->targetRange().min() + x + targetDelta ) - scale->mapFrom( scale->targetRange().min() + x ) ) / ( scale->subTicks() + 1 );
            for ( unsigned int j = 1; j <= scale->subTicks() && i < totalTicks; ++j )
            {
                qreal xx = scale->mapTo( scale->mapFrom( x ) + j * subDelta );
                painter->drawLine( QPointF( xx, yy1 ), QPointF( xx, yy2 ) );
            }
        }

        if ( scale->labelsVisible() )
        {
            qreal space = qAbs( y2 ) + 4;
            QRectF bbox;
            painter->save();

            if ( orientation == Qt::Vertical )
                painter->rotate( -90 );

            for ( unsigned int i = 0; i <= totalTicks; ++i )
            {
                QString label = QString( "%1" ).arg( scale->mapFrom( scale->targetRange().min() + targetDelta * i ), 0, 'f', 2 );
                bbox = painter->boundingRect( QRectF( 0, 0, length / totalTicks, 0 ), label );

                if ( orientation == Qt::Horizontal )
                {
                    bbox.moveLeft( - bbox.width() / 2 );
                    if ( !inverted )
                        bbox.moveTop( space );
                    else
                        bbox.moveTop( -( bbox.height() + space ) );
                }
                else
                {
                    bbox.moveTop( - bbox.height() / 2 );
                    if ( !inverted )
                        bbox.moveLeft( -( bbox.width() + space ) );
                    else
                        bbox.moveLeft( space );
                }

                painter->drawText( bbox, label );

                if ( orientation == Qt::Horizontal )
                    painter->translate( targetDelta, 0 );
                else
                    painter->translate( 0, targetDelta );
            }
            painter->restore();
        }
    }
}
