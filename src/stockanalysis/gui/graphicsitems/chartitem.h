#ifndef CHARTITEM_H
#define CHARTITEM_H

#include <QtWidgets/QGraphicsItem>
#include <QSharedPointer>
#include "scale.h"
#include "charttransform.h"

class ChartItem : public QGraphicsItem
{
public:
    explicit ChartItem( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent = 0 );

    QRectF rect() const;
    void setRect( const QRectF & rect );
    void setRect( qreal x, qreal y, qreal width, qreal height );

    QSharedPointer<Scale> scaleX() const;
    void setScaleX( const QSharedPointer<Scale> & scaleX );

    QSharedPointer<Scale> scaleY() const;
    void setScaleY( const QSharedPointer<Scale> & scaleY );

    QSharedPointer<ChartTransform> transform() const;

    virtual QRectF boundingRect() const;

private:
    QRectF m_rect;
    QSharedPointer<Scale> m_scaleX;
    QSharedPointer<Scale> m_scaleY;
    QSharedPointer<ChartTransform> m_transform;
};

#endif // CHARTITEM_H
