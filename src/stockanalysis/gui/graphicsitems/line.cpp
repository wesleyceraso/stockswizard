#include "line.h"
#include <QPainter>

Line::Line( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent ):
    ChartItem( scaleX, scaleY, parent ),
    m_pen( Qt::black )
{
}

Line::Line( QVector< QPointF > points, QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent ):
    ChartItem( scaleX, scaleY, parent )
{
    setPoints( points );
}

Line::Line( QList< QPointF > points, QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent ):
    ChartItem( scaleX, scaleY, parent )
{
    setPoints( points );
}

void Line::setPoints( const QVector< QPointF > & points )
{
    m_points = points;
}

void Line::setPoints( const QList< QPointF > & points )
{
    m_points = points.toVector();
}

QPen Line::pen() const
{
    return m_pen;
}

void Line::setPen(const QPen & pen)
{
    m_pen = pen;
}

void Line::paint( QPainter * painter, const QStyleOptionGraphicsItem *, QWidget * )
{
    painter->save();
    painter->setClipRect( boundingRect() );
    painter->setPen( m_pen );

    QVector< QPointF > ajustedPoints = transform()->mapTo( m_points );

    QPainterPath path;
    path.moveTo( ajustedPoints.first() );
    for ( int i = 1; i < ajustedPoints.size(); ++i )
    {
        path.lineTo( ajustedPoints.at( i ) );
    }

    painter->drawPath( path );
    painter->restore();
}
