#ifndef SCALE_H
#define SCALE_H

#include <QObject>
#include <QStringList>
#include <QSharedPointer>
#include "chartscalestrategy.h"

class Scale : public QObject
{
    Q_OBJECT

public:
    enum TickPosition {
        IN,
        OUT,
        IN_OUT
    };

public:
    Scale( QObject * parent = 0 );

    unsigned int ticks() const;
    void setTicks( unsigned int value );

    unsigned int subTicks() const;
    void setSubTicks( unsigned int value );

    QStringList labels() const;
    void setLabels( const QStringList &value );

    bool labelsVisible() const;
    void setLabelsVisible(bool labelsVisible);

    bool isVisible() const;
    void setVisible( bool value );

    Range<qreal> originRange() const;
    void setOriginRange( qreal min, qreal max );
    void setOriginRange( const Range<qreal> & range );

    Range<qreal> targetRange() const;
    void setTargetRange( qreal min, qreal max );
    void setTargetRange( const Range<qreal> & range );

    unsigned int tickSize() const;
    void setTickSize( unsigned int tickSize );

    unsigned int subTickSize() const;
    void setSubTickSize( unsigned int subTickSize );

    TickPosition tickPosition() const;
    void setTickPosition( const TickPosition & tickPosition );

    TickPosition subTickPosition() const;
    void setSubTickPosition( const TickPosition & subTickPosition );

    QSharedPointer< ChartScaleStrategy > scaleStrategy() const;
    void setScaleStrategy( const QSharedPointer< ChartScaleStrategy > & scaleStrategy );

    qreal mapTo( qreal value ) const;
    qreal mapFrom( qreal value ) const;

private:
    unsigned int m_ticks;
    unsigned int m_subTicks;
    QStringList  m_labels;
    bool m_labelsVisible;
    bool m_visible;
    Range<qreal> m_originRange;
    Range<qreal> m_targetRange;
    unsigned int m_tickSize;
    unsigned int m_subTickSize;
    TickPosition m_tickPosition;
    TickPosition m_subTickPosition;
    QSharedPointer< ChartScaleStrategy > m_scaleStrategy;
};

#endif // SCALE_H
