#include "chartarrow.h"
#include <QPainter>

ChartArrow::ChartArrow( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, ChartItem * parent ):
    ChartItem( scaleX, scaleY, parent )
{
    initializePolygon();
}

ChartArrow::ChartArrow( QSharedPointer<Scale> scaleX, QSharedPointer<Scale> scaleY, QPointF origin, qreal angle, ChartItem * parent ):
    ChartItem( scaleX, scaleY, parent ),
    m_origin( origin ),
    m_angle( angle )
{
    initializePolygon();
}

QPointF ChartArrow::origin() const
{
    return m_origin;
}

void ChartArrow::setOrigin(const QPointF & origin)
{
    m_origin = origin;
}

qreal ChartArrow::angle() const
{
    return m_angle;
}

void ChartArrow::setAngle(const qreal & angle)
{
    m_angle = angle;
}

void ChartArrow::paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget )
{
    painter->save();
    painter->setBrush( m_brush );
    painter->setPen( m_pen );

    painter->translate( transform()->mapTo( m_origin ) );
    painter->rotate( m_angle );

    painter->drawPolygon( m_polygon );

    painter->restore();
}

void ChartArrow::initializePolygon()
{
    m_polygon.append( QPointF( 0, 0 ) );
    m_polygon.append( QPointF( 10, 10 ) );
    m_polygon.append( QPointF( 10, 5 ) );
    m_polygon.append( QPointF( 30, 5 ) );
    m_polygon.append( QPointF( 30, -5 ) );
    m_polygon.append( QPointF( 10, -5 ) );
    m_polygon.append( QPointF( 10, -10 ) );
    m_polygon.append( QPointF( 0, 0 ) );
}

QBrush ChartArrow::brush() const
{
    return m_brush;
}

void ChartArrow::setBrush(const QBrush & brush)
{
    m_brush = brush;
}

QPen ChartArrow::pen() const
{
    return m_pen;
}

void ChartArrow::setPen(const QPen & pen)
{
    m_pen = pen;
}
