#include "scale.h"
#include "chartlinearscalestrategy.h"

Scale::Scale( QObject * parent ):
    QObject( parent ),
    m_ticks( 5 ),
    m_subTicks( 1 ),
    m_labels( QStringList() ),
    m_labelsVisible( true ),
    m_visible( true ),
    m_originRange( 0., 0. ),
    m_targetRange( 0., 0. ),
    m_tickSize( 5 ),
    m_subTickSize( 3 ),
    m_tickPosition( IN_OUT ),
    m_subTickPosition( IN_OUT ),
    m_scaleStrategy( new ChartLinearScaleStrategy )
{
}

unsigned int Scale::ticks() const
{
    return m_ticks;
}

void Scale::setTicks( unsigned int value )
{
    m_ticks = value;
}

unsigned int Scale::subTicks() const
{
    return m_subTicks;
}

void Scale::setSubTicks( unsigned int value )
{
    m_subTicks = value;
}

QStringList Scale::labels() const
{
    return m_labels;
}

void Scale::setLabels( const QStringList &value )
{
    m_labels = value;
}

bool Scale::isVisible() const
{
    return m_visible;
}

void Scale::setVisible( bool value )
{
    m_visible = value;
}

bool Scale::labelsVisible() const
{
    return m_labelsVisible;
}

void Scale::setLabelsVisible(bool labelsVisible)
{
    m_labelsVisible = labelsVisible;
}

Range<qreal> Scale::originRange() const
{
    return m_originRange;
}

void Scale::setOriginRange( qreal min, qreal max )
{
    m_originRange = Range<qreal>( min, max );
}

void Scale::setOriginRange( const Range<qreal> & range )
{
    m_originRange = range;
}

Range<qreal> Scale::targetRange() const
{
    return m_targetRange;
}

void Scale::setTargetRange( qreal min, qreal max )
{
    m_targetRange = Range<qreal>( min, max );
}

void Scale::setTargetRange( const Range<qreal> & range )
{
    m_targetRange = range;
}

unsigned int Scale::tickSize() const
{
    return m_tickSize;
}

void Scale::setTickSize( unsigned int tickSize )
{
    m_tickSize = tickSize;
}

unsigned int Scale::subTickSize() const
{
    return m_subTickSize;
}

void Scale::setSubTickSize( unsigned int subTickSize )
{
    m_subTickSize = subTickSize;
}

Scale::TickPosition Scale::tickPosition() const
{
    return m_tickPosition;
}

void Scale::setTickPosition( const Scale::TickPosition & tickPosition )
{
    m_tickPosition = tickPosition;
}

Scale::TickPosition Scale::subTickPosition() const
{
    return m_subTickPosition;
}

void Scale::setSubTickPosition( const Scale::TickPosition & subTickPosition )
{
    m_subTickPosition = subTickPosition;
}

QSharedPointer< ChartScaleStrategy > Scale::scaleStrategy() const
{
    return m_scaleStrategy;
}

void Scale::setScaleStrategy( const QSharedPointer< ChartScaleStrategy > & scaleStrategy )
{
    m_scaleStrategy = scaleStrategy;
}

qreal Scale::mapTo( qreal value ) const
{
    return scaleStrategy()->mapTo( value, m_originRange, m_targetRange );
}

qreal Scale::mapFrom( qreal value ) const
{
    return scaleStrategy()->mapTo( value, m_targetRange, m_originRange );
}

