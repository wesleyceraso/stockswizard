#ifndef CHARTLINEARSCALESTRATEGY_H
#define CHARTLINEARSCALESTRATEGY_H

#include "chartscalestrategy.h"

class ChartLinearScaleStrategy : public ChartScaleStrategy
{
public:
    ChartLinearScaleStrategy();

    virtual qreal mapTo( qreal, const Range<qreal> & originRange, const Range<qreal> & targetRange ) const;
};

#endif // CHARTLINEARSCALESTRATEGY_H
