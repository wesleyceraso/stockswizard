#include "chartscalestrategy.h"

ChartScaleStrategy::ChartScaleStrategy()
{
}

QList<qreal> ChartScaleStrategy::mapTo( const QList<qreal> & points, const Range<qreal> & originRange, const Range<qreal> & targetRange ) const
{
    QList<qreal> result;
    for ( qreal p: points )
    {
        result.append( mapTo( p, originRange, targetRange ) );
    }
    return result;
}

QVector<qreal> ChartScaleStrategy::mapTo( const QVector<qreal> & points, const Range<qreal> & originRange, const Range<qreal> & targetRange ) const
{
    QVector<qreal> result;
    for ( qreal p: points )
    {
        result.append( mapTo( p, originRange, targetRange ) );
    }
    return result;
}
