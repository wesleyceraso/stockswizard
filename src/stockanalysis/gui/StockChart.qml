import QtQuick 2.1
import "chart"
import "analysis"
import "analysis/ExponentialMovingAverage.js" as Lib

ChartXY {
    id: chart

    property string title
    property var model: []

    property ChartMapper mapper: ChartMapper {
        id: chartMapper
        horizontalAxis: chart.bottomAxis
        verticalAxis: chart.rightAxis
        viewport: chart.viewport
    }

    onModelChanged: requestPaint();

    QtObject {
        id: _internal
        property var max_min: Lib.findMaxMinQuotes(chart.model.slice(
                                                       Math.min(Math.max(mapper.horizontalAxis.min, 0), chart.model.length),
                                                       Math.min(Math.max(mapper.horizontalAxis.max, 0), chart.model.length)), 5, 5);
    }

    rightAxis {
        min: _internal.max_min[0]
        max: _internal.max_min[1]
    }

    drawableChildren: [
        ChartBarStick {
            id: barStick
            mapper: chart.mapper
            candles: chart.model
        },
        ChartLabel {
            /*labels: {
                var r = new Array;
                var p1 = new Object;
                p1.color = barStick.lineColor;
                p1.label = chart.title;
                r.push(p1);
                var p2 = new Object;
                p2.color = envelop.averageLineColor;
                p2.label = "MME " + envelop.period;
                r.push(p2);
                var p3 = new Object;
                p3.color = envelop.parallelLinesColor;
                p3.label = "Envelop " + envelop.increment + "%";
                r.push(p3);
                var p4 = new Object;
                p4.color = safeZone.lineColor;
                p4.label = "SafeZone " + safeZone.period + " " + safeZone.multiplier.toFixed(1) + "x";
                r.push(p4);
                return r;
            }*/

            mapper: chart.mapper
        }
    ]
}
