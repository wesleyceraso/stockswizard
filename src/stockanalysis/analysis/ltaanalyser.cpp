#include "ltaanalyser.h"
#include "valleyanalyser.h"
#include "peakanalyser.h"
#include "utils.h"
#include <cmath>

std::vector<TendencyLine> LtaAnalyser::process(const std::vector<Quote> & quotes)
{
    std::vector<unsigned int> valleys = ValleyAnalyser::process(quotes);
    std::vector<unsigned int>::const_iterator valleyIt = valleys.begin();

    std::vector<TendencyLine> tendencyLines;

    if (!valleys.empty())
    {
        unsigned int firstPivot = *valleyIt;
        unsigned int currentPivot = firstPivot;
        while ( ++valleyIt != valleys.end() )
        {
            double coeficient = (quotes.at(*valleyIt).low() - quotes.at(currentPivot).low()) / (*valleyIt - currentPivot);
            if (coeficient > 0.)
            {
                currentPivot = *valleyIt;
            }
            else
            {
                if (currentPivot - firstPivot)
                {
                    TendencyLine tl;
                    tl.startQuoteIndex = firstPivot;
                    tl.relevance = currentPivot - firstPivot;
                    tendencyLines.push_back(tl);
                }
                firstPivot = *valleyIt;
                currentPivot = firstPivot;
            }
        }

        if (currentPivot - firstPivot)
        {
            TendencyLine tl;
            tl.startQuoteIndex = firstPivot;
            tl.relevance = currentPivot - firstPivot;
            tendencyLines.push_back(tl);
        }
    }

    return tendencyLines;
}

/*std::list< TendencyLine > LtaAnalyser::process( const std::list< Quote > & quotesList )
{
    std::vector< Quote > quotes = toVector( quotesList );
    std::list< unsigned int > valleys = ValleyAnalyser::process( quotesList );

    std::list< TendencyLine > tendencyLines;

    double lowestCoeficientSoFar = 0.;
    unsigned int relevance = 0;
    std::list< unsigned int >::const_iterator itStart = valleys.begin();
    for ( std::list< unsigned int >::const_iterator it = valleys.begin(); it != valleys.end(); ++it )
    {
        double coeficient = ( quotes[ *it ].close() - quotes[ *itStart ].close() ) / ( *it - *itStart );

        if ( coeficient > 0 )
        {
            lowestCoeficientSoFar = lowestCoeficientSoFar != 0. ? std::min( coeficient, lowestCoeficientSoFar ) : coeficient;
            ++relevance;
        }
        else
        {
            if ( lowestCoeficientSoFar != 0. )
            {
                tendencyLines.push_back( TendencyLine{ *itStart, atan( lowestCoeficientSoFar ), relevance } );
            }
            itStart = it;
            lowestCoeficientSoFar = 0;
            relevance = 0;
        }
    }

    if ( lowestCoeficientSoFar != 0. )
    {
        tendencyLines.push_back( TendencyLine{ *itStart, atan( lowestCoeficientSoFar ), relevance } );
    }

    return tendencyLines;
}*/
