#include "movingaverageanalyser.h"
#include <movingaverage.h>

std::vector<price> MovingAverageAnalyser::processOpen(const std::vector<Quote> & vector, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: vector)
    {
        toBeProcessed.push_back(q.open());
    }
    return MovingAverage::process(toBeProcessed, period);
}

std::vector<price> MovingAverageAnalyser::processClose(const std::vector<Quote> & vector, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: vector)
    {
        toBeProcessed.push_back(q.close());
    }
    return MovingAverage::process(toBeProcessed, period);
}

std::vector<price> MovingAverageAnalyser::processAverage(const std::vector<Quote> & vector, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: vector)
    {
        toBeProcessed.push_back(q.average());
    }
    return MovingAverage::process(toBeProcessed, period);
}

std::vector<price> MovingAverageAnalyser::processHigh(const std::vector<Quote> & vector, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: vector)
    {
        toBeProcessed.push_back(q.high());
    }
    return MovingAverage::process(toBeProcessed, period);
}

std::vector<price> MovingAverageAnalyser::processLow(const std::vector<Quote> & vector, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: vector)
    {
        toBeProcessed.push_back(q.low());
    }
    return MovingAverage::process(toBeProcessed, period);
}
