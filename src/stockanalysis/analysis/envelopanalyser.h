#ifndef ENVELOPANALYSER_H
#define ENVELOPANALYSER_H

#include <vector>
#include "../../stockexchange/quote.h"

class EnvelopAnalyser
{
public:
    static std::pair<std::vector<price>, double> process(const std::vector<Quote> & quotes, unsigned int period);

private:
    static std::vector<price> shiftedVector(const std::vector<price> & lhs, price offset);
};

#endif // ENVELOPANALYSER_H
