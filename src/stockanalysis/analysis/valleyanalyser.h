#ifndef VALLEYANALYSER_H
#define VALLEYANALYSER_H

#include <quote.h>
#include <vector>

class ValleyAnalyser
{
public:
    static std::vector<unsigned int> process(const std::vector<Quote> &);
};

#endif // VALLEYANALYSER_H
