#ifndef LTAANALYSER_H
#define LTAANALYSER_H

#include <quote.h>
#include <vector>

typedef struct tendencyLine
{
    unsigned int startQuoteIndex;
    double       angle;
    unsigned int relevance;
} TendencyLine;

class LtaAnalyser
{
public:
    static std::vector<TendencyLine> process(const std::vector<Quote> &);
};

#endif // LTAANALYSER_H
