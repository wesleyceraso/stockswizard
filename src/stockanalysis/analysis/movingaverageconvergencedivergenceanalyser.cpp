#include "movingaverageconvergencedivergenceanalyser.h"
#include "algorithm/exponentialmovingaverage.h"
#include "exponentialmovingaverageanalyser.h"

std::pair<std::vector<price>, std::vector<price> > MovingAverageConvergenceDivergenceAnalyser::processOpen(const std::vector<Quote> &quotes, unsigned int period1, unsigned int period2, unsigned int period3)
{
    std::vector<price> fastLine = subtractVectorComponents(ExponentialMovingAverageAnalyser::processOpen(quotes, period2),
                                                  ExponentialMovingAverageAnalyser::processOpen(quotes, period1));
    std::vector<price> slowLine = ExponentialMovingAverage::process(fastLine, period3);

    return make_pair(fastLine, slowLine);
}

std::pair<std::vector<price>, std::vector<price> > MovingAverageConvergenceDivergenceAnalyser::processClose(const std::vector<Quote> &quotes, unsigned int period1, unsigned int period2, unsigned int period3)
{
    std::vector<price> fastLine = subtractVectorComponents(ExponentialMovingAverageAnalyser::processClose(quotes, period2),
                                                  ExponentialMovingAverageAnalyser::processClose(quotes, period1));
    std::vector<price> slowLine = ExponentialMovingAverage::process(fastLine, period3);

    return make_pair(fastLine, slowLine);
}

std::pair<std::vector<price>, std::vector<price> > MovingAverageConvergenceDivergenceAnalyser::processAverage(const std::vector<Quote> &quotes, unsigned int period1, unsigned int period2, unsigned int period3)
{
    std::vector<price> fastLine = subtractVectorComponents(ExponentialMovingAverageAnalyser::processAverage(quotes, period2),
                                                  ExponentialMovingAverageAnalyser::processAverage(quotes, period1));
    std::vector<price> slowLine = ExponentialMovingAverage::process(fastLine, period3);

    return make_pair(fastLine, slowLine);
}

std::pair<std::vector<price>, std::vector<price> > MovingAverageConvergenceDivergenceAnalyser::processHigh(const std::vector<Quote> &quotes, unsigned int period1, unsigned int period2, unsigned int period3)
{
    std::vector<price> fastLine = subtractVectorComponents(ExponentialMovingAverageAnalyser::processAverage(quotes, period2),
                                                  ExponentialMovingAverageAnalyser::processAverage(quotes, period1));
    std::vector<price> slowLine = ExponentialMovingAverage::process(fastLine, period3);

    return make_pair(fastLine, slowLine);
}

std::pair<std::vector<price>, std::vector<price> > MovingAverageConvergenceDivergenceAnalyser::processLow(const std::vector<Quote> &quotes, unsigned int period1, unsigned int period2, unsigned int period3)
{
    std::vector<price> fastLine = subtractVectorComponents(ExponentialMovingAverageAnalyser::processLow(quotes, period2),
                                                  ExponentialMovingAverageAnalyser::processLow(quotes, period1));
    std::vector<price> slowLine = ExponentialMovingAverage::process(fastLine, period3);

    return make_pair(fastLine, slowLine);
}

std::vector<price> MovingAverageConvergenceDivergenceAnalyser::subtractVectorComponents(const std::vector<price> &lhs, const std::vector<price> &rhs)
{
    std::vector<price> result;
    unsigned int offset = lhs.size() - rhs.size();
    for (unsigned int i=0; i<rhs.size(); ++i)
    {
        result.push_back(lhs[i+offset] - rhs[i]);
    }
    return result;
}
