#include "envelopanalyser.h"
#include "../../algorithm/exponentialmovingaverage.h"
#include "exponentialmovingaverageanalyser.h"
#include "peakanalyser.h"
#include "valleyanalyser.h"

std::pair<std::vector<price>, double> EnvelopAnalyser::process(const std::vector<Quote> &quotes, unsigned int period)
{
    std::vector<price> mme = ExponentialMovingAverageAnalyser::processAverage(quotes, period);

    std::vector<price> diffToPeaks;
    for (unsigned int index: PeakAnalyser::process(quotes))
    {
        if (index < period) continue;
        diffToPeaks.push_back(quotes[index].high() - mme[index - period]);
    }

    std::vector<price> diffToValleys;
    for (unsigned int index: ValleyAnalyser::process(quotes))
    {
        if (index < period) continue;
        diffToValleys.push_back(mme[index - period] - quotes[index].low());
    }

    price upperDiff = ExponentialMovingAverage::process(diffToPeaks, period).front();
    price lowerDiff = ExponentialMovingAverage::process(diffToValleys, period).front();

    price diff = (upperDiff + lowerDiff) / 2;

    return make_pair(mme, diff / mme.back() * 100);
}

std::vector<price> EnvelopAnalyser::shiftedVector(const std::vector<price> &lhs, price offset)
{
    std::vector<price> result;
    for (price p: lhs)
    {
        result.push_back(p + offset);
    }
    return result;
}
