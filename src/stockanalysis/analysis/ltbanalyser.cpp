#include "ltbanalyser.h"
#include "peakanalyser.h"
#include "utils.h"
#include <cmath>

std::vector<TendencyLine> LtbAnalyser::process(const std::vector<Quote> & quotes)
{
    std::vector<unsigned int> peaks = PeakAnalyser::process(quotes);
    std::vector<unsigned int>::const_iterator peakIt = peaks.begin();

    std::vector<TendencyLine> tendencyLines;

    if (!peaks.empty())
    {
        unsigned int firstPivot = *peakIt;
        unsigned int currentPivot = firstPivot;
        while (++peakIt != peaks.end())
        {
            double coeficient = (quotes.at(*peakIt).high() - quotes.at(currentPivot).high()) / (*peakIt - currentPivot);
            if (coeficient < 0.)
            {
                currentPivot = *peakIt;
            }
            else
            {
                if (currentPivot - firstPivot)
                {
                    TendencyLine tl;
                    tl.startQuoteIndex = firstPivot;
                    tl.relevance = currentPivot - firstPivot;
                    tendencyLines.push_back(tl);
                }
                firstPivot = *peakIt;
                currentPivot = firstPivot;
            }
        }

        if (currentPivot - firstPivot)
        {
            TendencyLine tl;
            tl.startQuoteIndex = firstPivot;
            tl.relevance = currentPivot - firstPivot;
            tendencyLines.push_back(tl);
        }
    }

    return tendencyLines;
}

/*std::list< TendencyLine > LtbAnalyser::process( const std::list< Quote > & quotesList )
{
    std::vector< Quote > quotes = toVector( quotesList );
    std::list< unsigned int > peaks = PeakAnalyser::process( quotesList );

    std::list< TendencyLine > tendencyLines;

    double lowestCoeficientSoFar = 0.;
    unsigned int relevance = 0;
    std::list< unsigned int >::const_iterator itStart = peaks.begin();
    for ( std::list< unsigned int >::const_iterator it = peaks.begin(); it != peaks.end(); ++it )
    {
        double coeficient = ( quotes[ *it ].close() - quotes[ *itStart ].close() ) / ( *it - *itStart );

        if ( coeficient < 0 )
        {
            lowestCoeficientSoFar = lowestCoeficientSoFar != 0. ? std::max( coeficient, lowestCoeficientSoFar ) : coeficient;
            ++relevance;
        }
        else
        {
            if ( lowestCoeficientSoFar != 0. )
            {
                tendencyLines.push_back( TendencyLine{ *itStart, atan( lowestCoeficientSoFar ), relevance } );
            }
            itStart = it;
            lowestCoeficientSoFar = 0;
            relevance = 0;
        }
    }

    if ( lowestCoeficientSoFar != 0. )
    {
        tendencyLines.push_back( TendencyLine{ *itStart, atan( lowestCoeficientSoFar ), relevance } );
    }

    return tendencyLines;
}*/
