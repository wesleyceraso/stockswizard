#ifndef EXPONENTIALMOVINGAVERAGEANALYSER_H
#define EXPONENTIALMOVINGAVERAGEANALYSER_H

#include <vector>
#include <quote.h>

class ExponentialMovingAverageAnalyser
{
public:
    static std::vector<price> processOpen   (const std::vector<Quote> & quotes, unsigned int period);
    static std::vector<price> processClose  (const std::vector<Quote> & quotes, unsigned int period);

    static std::vector<price> processAverage(const std::vector<Quote> & quotes, unsigned int period);

    static std::vector<price> processHigh   (const std::vector<Quote> & quotes, unsigned int period);
    static std::vector<price> processLow    (const std::vector<Quote> & quotes, unsigned int period);
};

#endif // EXPONENTIALMOVINGAVERAGEANALYSER_H
