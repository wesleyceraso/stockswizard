#ifndef LTBANALYSER_H
#define LTBANALYSER_H

#include "ltaanalyser.h"

class LtbAnalyser
{
public:
    static std::vector<TendencyLine> process(const std::vector<Quote> &);
};

#endif // LTBANALYSER_H
