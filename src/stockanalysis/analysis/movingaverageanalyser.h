#ifndef MOVINGAVERAGEANALYSER_H
#define MOVINGAVERAGEANALYSER_H

#include <vector>
#include <quote.h>

class MovingAverageAnalyser
{
public:
    static std::vector<price> processOpen   (const std::vector<Quote> & vector, unsigned int period );
    static std::vector<price> processClose  (const std::vector<Quote> & vector, unsigned int period );

    static std::vector<price> processAverage(const std::vector<Quote> & vector, unsigned int period );

    static std::vector<price> processHigh   (const std::vector<Quote> & vector, unsigned int period );
    static std::vector<price> processLow    (const std::vector<Quote> & vector, unsigned int period );
};

#endif // MOVINGAVERAGEANALYSER_H
