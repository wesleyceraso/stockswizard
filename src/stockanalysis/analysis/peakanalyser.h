#ifndef PEAKANALYSER_H
#define PEAKANALYSER_H

#include <quote.h>
#include <vector>

class PeakAnalyser
{
public:
    static std::vector<unsigned int> process(const std::vector<Quote> &);
};

#endif // PEAKANALYSER_H
