#include "peakanalyser.h"

std::vector<unsigned int> PeakAnalyser::process(const std::vector<Quote> & quotes)
{
    if (quotes.size() < 3)
        return std::vector<unsigned int>();

    std::vector<unsigned int> indexes;

    std::vector<Quote>::const_iterator next = quotes.begin();
    std::vector<Quote>::const_iterator previous = next++;
    std::vector<Quote>::const_iterator current = next++;

    if (previous->high() >= current->high())
    {
        indexes.push_back( 0 );
    }

    for (unsigned int index = 1; next != quotes.end(); ++previous, ++current, ++next, ++index)
    {
        if (current->high() > previous->high() && current->high() >= next->high())
        {
            indexes.push_back(index);
        }
    }

    if (current->high() > previous->high())
    {
        indexes.push_back(quotes.size() - 1);
    }

    return indexes;
}
