#ifndef MOVINGAVERAGECONVERGENCEDIVERGENCEANALYSER_H
#define MOVINGAVERAGECONVERGENCEDIVERGENCEANALYSER_H

#include <vector>
#include <utility>
#include "stockexchange/quote.h"

class MovingAverageConvergenceDivergenceAnalyser
{
public:
    static std::pair<std::vector<price>, std::vector<price> > processOpen   (const std::vector<Quote> & quotes, unsigned int period1 = 9, unsigned int period2 = 12, unsigned int period3 = 26);
    static std::pair<std::vector<price>, std::vector<price> > processClose  (const std::vector<Quote> & quotes, unsigned int period1 = 9, unsigned int period2 = 12, unsigned int period3 = 26);

    static std::pair<std::vector<price>, std::vector<price> > processAverage(const std::vector<Quote> & quotes, unsigned int period1 = 9, unsigned int period2 = 12, unsigned int period3 = 26);

    static std::pair<std::vector<price>, std::vector<price> > processHigh   (const std::vector<Quote> & quotes, unsigned int period1 = 9, unsigned int period2 = 12, unsigned int period3 = 26);
    static std::pair<std::vector<price>, std::vector<price> > processLow    (const std::vector<Quote> & quotes, unsigned int period1 = 9, unsigned int period2 = 12, unsigned int period3 = 26);

private:
    static std::vector<price> subtractVectorComponents(const std::vector<price> & lhs, const std::vector<price> & rhs);
};

#endif // MOVINGAVERAGECONVERGENCEDIVERGENCEANALYSER_H
