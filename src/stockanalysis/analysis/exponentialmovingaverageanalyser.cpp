#include "exponentialmovingaverageanalyser.h"
#include "algorithm/exponentialmovingaverage.h"

std::vector<price> ExponentialMovingAverageAnalyser::processOpen(const std::vector<Quote> & quotes, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: quotes)
    {
        toBeProcessed.push_back(q.open());
    }
    return ExponentialMovingAverage::process(toBeProcessed, period);
}

std::vector<price> ExponentialMovingAverageAnalyser::processClose(const std::vector<Quote> & quotes, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: quotes)
    {
        toBeProcessed.push_back(q.close());
    }
    return ExponentialMovingAverage::process(toBeProcessed, period);
}

std::vector<price> ExponentialMovingAverageAnalyser::processAverage(const std::vector<Quote> & quotes, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: quotes)
    {
        toBeProcessed.push_back(q.average());
    }
    return ExponentialMovingAverage::process(toBeProcessed, period);
}

std::vector<price> ExponentialMovingAverageAnalyser::processHigh(const std::vector<Quote> & quotes, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: quotes)
    {
        toBeProcessed.push_back(q.high());
    }
    return ExponentialMovingAverage::process(toBeProcessed, period);
}

std::vector<price> ExponentialMovingAverageAnalyser::processLow(const std::vector<Quote> & quotes, unsigned int period)
{
    std::vector<price> toBeProcessed;
    for (const Quote & q: quotes)
    {
        toBeProcessed.push_back(q.low());
    }
    return ExponentialMovingAverage::process(toBeProcessed, period);
}
