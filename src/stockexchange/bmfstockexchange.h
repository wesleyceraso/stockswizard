#ifndef BMFSTOCKEXCHANGE_H
#define BMFSTOCKEXCHANGE_H

#include <list>
#include <string>

#include "quote.h"
#include "stock.h"
#include "tradingday.h"

class BmfStockExchange
{
public:
    BmfStockExchange();

    std::list<Quote> quotes(const Stock * = 0, const TradingDay * = 0) const;
    std::list<Stock> stocks() const;
    Stock stock(const std::string &) const;
    std::list<TradingDay> tradingDays() const;

    void setContentFromFile(const std::string &filePath);

private:
    std::list<Quote> m_quotes;
    std::list<Stock> m_stocks;
    std::list<TradingDay> m_tradingDays;
};

#endif // BMFSTOCKEXCHANGE_H
