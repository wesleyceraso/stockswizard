#include "stock.h"
#include <cstring>

Stock::Stock( const std::string & code, const std::string & name, const std::string & specification, int correctionFactor ):
    m_code( code ),
    m_name( name ),
    m_specification( specification ),
    m_correctionFactor( correctionFactor )

{
}

std::string Stock::code() const
{
    return m_code;
}

std::string Stock::name() const
{
    return m_name;
}

std::string Stock::specification() const
{
    return m_specification;
}

int Stock::correctionFactor() const
{
    return m_correctionFactor;
}

bool Stock::operator==( const Stock & rhs ) const
{
    return m_code == rhs.m_code;
}

bool Stock::operator<(const Stock & rhs) const
{
    return (std::strcmp(m_code.data(), rhs.m_code.data()) < 0);
}

std::ostream & operator<<( std::ostream & o, const Stock & rhs )
{
    o <<
            "Code:              " << rhs.m_code             << std::endl <<
            "Name:              " << rhs.m_name             << std::endl <<
            "Specification:     " << rhs.m_specification    << std::endl <<
            "CorrectionFactor:  " << rhs.m_correctionFactor << std::endl;
    return o;
}
