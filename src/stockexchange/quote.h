#ifndef QUOTE_H
#define QUOTE_H

#include "stock.h"
#include "tradingday.h"
#include <ostream>

typedef double price;

typedef struct quoteStr {
    price open;
    price close;
    price average;
    price high;
    price low;
    int   negotiations;
    price tradingVolume;
    int   stocksTraded;
    const Stock stock;
    const TradingDay tradingDay;
} QuoteInitializer;

class Quote
{
public:
    explicit Quote( const QuoteInitializer & );

    price open () const;
    price close() const;

    price average() const;

    price high () const;
    price low  () const;

    int   negotiations() const;
    price tradingVolume() const;
    int   stocksTraded() const;

    Stock stock() const;
    TradingDay tradingDay() const;

    friend std::ostream & operator<<( std::ostream &, const Quote & );

private:
    price m_open;
    price m_close;

    price m_average;

    price m_high;
    price m_low;

    int   m_negotiations;
    price m_tradingVolume;
    int   m_stocksTraded;

    Stock m_stock;
    TradingDay m_tradingDay;
};

#endif // QUOTE_H
