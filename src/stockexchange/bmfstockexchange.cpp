#include "bmfstockexchange.h"
#include <bmffileparser.h>
#include <bmffileregister01.h>
#include <fstream>
#include <set>
#include <algorithm>
#include <iostream>

struct PointerParam
{
    const Stock * stockFilter;
    const TradingDay * tradingDayFilter;
};

BmfStockExchange::BmfStockExchange()
{
}

std::list<Quote> BmfStockExchange::quotes(const Stock * stockFilter , const TradingDay * tradingDayFilter) const
{
    using namespace std;
    list<Quote> temp;

    for (const Quote & q: m_quotes)
    {
        if ((!stockFilter || q.stock() == *stockFilter) && (!tradingDayFilter || q.tradingDay() == *tradingDayFilter))
        {
            temp.push_back(q);
        }
    }

    return temp;
}

std::list< Stock > BmfStockExchange::stocks() const
{
    return m_stocks;
}

Stock BmfStockExchange::stock( const std::string & code ) const
{
    for ( std::list< Stock >::const_iterator it = m_stocks.begin(); it != m_stocks.end(); ++it )
    {
        if ( it->code() == code )
            return  *it;
    }
    return Stock( "", "", "", 0 );
}

std::list< TradingDay > BmfStockExchange::tradingDays() const
{
    return m_tradingDays;
}

void BmfStockExchange::setContentFromFile( const std::string & filePath )
{
    std::ifstream is;
    is.open(filePath, std::ifstream::binary | std::ifstream::in);

//    char buffer[10 * 245];
//    std::filebuf fb;
//    fb.open(filePath, std::ifstream::binary | std::ifstream::in);
//    fb.pubsetbuf(buffer, 10 * 245);

//    std::istream is(&fb);


//    std::ifstream aux;
//    aux.open(filePath, std::ifstream::binary | std::ifstream::in);
//    aux.seekg(0, std::ios_base::end);
//    int length = aux.tellg();
//    aux.seekg(0, std::ios_base::beg);
//    char * content = new char[length];
//    aux.read(content, length);
//    aux.close();

//    std::stringstream is(content);

    /*
    is.seekg( 0, std::ios_base::end );
    int length = is.tellg();
    is.seekg( 0, std::ios_base::beg );
    char * content = new char[ length ];
    is.read( content, length );
    std::cout << content << std::endl;
    */

    BmfFileParser parser( is );
    BmfFileRegister01 reg;

    std::set<Stock> stocksSet;
    std::set<TradingDay> tradingDaySet;

    while ( parser >> reg )
    {
        //std::cerr << "Registro encontrado: \n" << reg << std::endl;

        //filtragem do tipo de mercado, codigo BDI e moeda de referencia
        if ( reg.tipoMercado() != 10 || reg.codigoBdi() != "02" || reg.moedaReferencia() != "R$" )
        {
            //std::cerr << "Registro decartado: \n" << reg << std::endl;
            continue;
        }

        auto stockIt = stocksSet.emplace(reg.codigoNegociacao(), reg.nomeResumido(), reg.especificacao(), reg.fatorCorrecaoPapel());
        auto tradingIt = tradingDaySet.emplace(reg.dataPregao());

        Quote quote({
            reg.precoAbertura(),
            reg.precoFechamento(),
            reg.precoMedio(),
            reg.precoMaximo(),
            reg.precoMinimo(),
            reg.totalNegociacoes(),
            reg.volumeTotalNegociacoes(),
            reg.totalTitulosNegociados(),
            *stockIt.first,
            *tradingIt.first
        });
        m_quotes.push_back( quote );
    }

    for (const Stock & s: stocksSet)
    {
        m_stocks.push_back( s );
    }
    for (const TradingDay & t: tradingDaySet)
    {
        m_tradingDays.push_back( t );
    }

    is.close();
}
