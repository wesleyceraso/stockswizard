#include "quote.h"
#include "stock.h"
#include "tradingday.h"

Quote::Quote( const QuoteInitializer & init ):
    m_open( init.open ),
    m_close( init.close ),
    m_average( init.average ),
    m_high( init.high ),
    m_low( init.low ),
    m_negotiations( init.negotiations ),
    m_tradingVolume( init.tradingVolume ),
    m_stocksTraded( init.stocksTraded ),
    m_stock( init.stock ),
    m_tradingDay( init.tradingDay )
{
}

price Quote::open() const
{
    return m_open;
}

price Quote::close() const
{
    return m_close;
}

price Quote::average() const
{
    return m_average;
}

price Quote::high() const
{
    return m_high;
}

price Quote::low() const
{
    return m_low;
}

int Quote::negotiations() const
{
    return m_negotiations;
}

price Quote::tradingVolume() const
{
    return m_tradingVolume;
}

int Quote::stocksTraded() const
{
    return m_stocksTraded;
}

Stock Quote::stock() const
{
    return m_stock;
}

TradingDay Quote::tradingDay() const
{
    return m_tradingDay;
}

std::ostream & operator<<( std::ostream & o, const Quote & rhs )
{
    o <<
            "Open:          R$" << rhs.m_open     << std::endl <<
            "Close:         R$" << rhs.m_close    << std::endl <<
            "Medium:        R$" << rhs.m_average  << std::endl <<
            "High:          R$" << rhs.m_high     << std::endl <<
            "Low:           R$" << rhs.m_low      << std::endl <<
            "Negotiations:  " << rhs.m_negotiations     << std::endl <<
            "TradingVolume: R$" << rhs.m_tradingVolume    << std::endl <<
            "StocksTraded:  " << rhs.m_stocksTraded     << std::endl;
    return o;
}
