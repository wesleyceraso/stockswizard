#ifndef TRADINGDAY_H
#define TRADINGDAY_H

#include <date.h>
#include <ostream>

class TradingDay
{
public:
    TradingDay( const Date & date );

    Date date() const;

    bool operator==( const TradingDay & ) const;
    bool operator<( const TradingDay & ) const;

    friend std::ostream & operator<<( std::ostream &, const TradingDay & );

private:
    Date m_date;
};

#endif // TRADINGDAY_H
