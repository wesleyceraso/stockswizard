#ifndef BMFFILEPARSER_H
#define BMFFILEPARSER_H

#include <string>
#include <sstream>

class BmfFileRegister01;

class BmfFileParser
{
public:
    explicit BmfFileParser( std::istream & istream );

    BmfFileParser & operator>>( BmfFileRegister01 & stock );

    /**
     * Implicity conversion to bool
     */
    operator bool() const;

private:
    BmfFileParser( const BmfFileParser & );
    BmfFileParser & operator=( const BmfFileParser & );

    std::istream & m_dataStream;
};

#endif // BMFFILEPARSER_H
