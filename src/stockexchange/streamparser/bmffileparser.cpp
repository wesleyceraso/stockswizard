#include "bmffileparser.h"
#include <cstring>
#include <cstdlib>
#include "bmffileregister01.h"

//GENERAL
const int BMF_BLOCK_SIZE = 245;
const int BMF_TIPREG_SIZE = 2;

//HEADER
const int BMF_NOME_ARQUIVO  = 13;
const int BMF_CODIGO_ORIGEM = 8;
const int BMF_DATA_GERACAO  = 8;
const int BMF_FILLER        = 214;


//TRAILER

const char headerTIPREG[]  = "00";
const char quotesTIPREG[]  = "01";
const char trailerTIPREG[] = "99";

BmfFileParser::BmfFileParser(  std::istream & istream ):
    m_dataStream( istream )
{
}

BmfFileParser & BmfFileParser::operator>>( BmfFileRegister01 & reg )
{
    char bmfBlock[ BMF_TIPREG_SIZE + 1 ];
    m_dataStream.read( bmfBlock, BMF_TIPREG_SIZE );
    bmfBlock[ BMF_TIPREG_SIZE ] = '\0';

    if ( !memcmp( quotesTIPREG, bmfBlock, BMF_TIPREG_SIZE ) )
    {
        reg = BmfFileRegister01::fromStream( m_dataStream );
        //\r\f
        m_dataStream.seekg( 2, std::ios_base::cur );
    }
    else if ( !memcmp( headerTIPREG, bmfBlock, BMF_TIPREG_SIZE ) )
    {
        m_dataStream.seekg( BMF_BLOCK_SIZE - BMF_TIPREG_SIZE, std::ios_base::cur );
        //\r\f
        m_dataStream.seekg( 2, std::ios_base::cur );
        return operator>>( reg );
    }
    else
    {
        m_dataStream.seekg( BMF_BLOCK_SIZE - BMF_TIPREG_SIZE, std::ios_base::cur );
        //\r\f
        m_dataStream.seekg( 2, std::ios_base::cur );
    }

    return *this;
}

BmfFileParser::operator bool() const
{
    return m_dataStream.good();
}
