#include "bmffileregister01.h"
#include <stringutils.h>
#include <cstdlib>
#include <istream>
#include <iomanip>

//QUOTE
const int BMF_DATA_PREGAO      = 8;
const int BMF_CODIGO_BDI       = 2;
const int BMF_CODIGO_NEGOCICAO = 12;
const int BMF_TIPO_MERCADO     = 3;
const int BMF_NOME_RESUMIDO    = 12;
const int BMF_ESPECIFICACAO    = 10;
const int BMF_PRAZO_TERMO      = 3;
const int BMF_MOEDA_REFERENCIA = 4;
const int BMF_PRECO_ABERTURA   = 13;
const int BMF_PRECO_MAXIMO     = 13;
const int BMF_PRECO_MINIMO     = 13;
const int BMF_PRECO_MEDIO      = 13;
const int BMF_PRECO_FECHAMENTO = 13;
const int BMF_PRECO_MELHOR_OFERTA_COMPRA = 13;
const int BMF_PRECO_MELHOR_OFERTA_VENDA  = 13;
const int BMF_TOTAL_NEGOCIACOES          = 5;
const int BMF_QUANTIDADE_TITULOS_NEGOCIADOS = 18;
const int BMF_VOLUME_TITULOS_NEGOCIADOS     = 18;
const int BMF_PRECO_OPCOES_TERMO     = 13;
const int BMF_CORRECAO_OPCOES_TERMO  = 1;
const int BMF_DATA_VENC_OPCOES_TERMO = 8;
const int BMF_FATOR_COTACAO          = 7;
const int BMF_PRECO_PONTOS           = 13;
const int BMF_CODIGO_ISIN            = 12;
const int BMF_NUMERO_DISTRIBUICAO    = 3;

BmfFileRegister01::BmfFileRegister01()
{
}

Date BmfFileRegister01::dataPregao() const
{
    return m_dataPregao;
}

std::string BmfFileRegister01::codigoBdi() const
{
    return m_codigoBdi;
}

std::string BmfFileRegister01::codigoNegociacao() const
{
    return m_codigoNegociacao;
}

int BmfFileRegister01::tipoMercado() const
{
    return m_tipoMercado;
}

std::string BmfFileRegister01::nomeResumido() const
{
    return m_nomeResumido;
}

std::string BmfFileRegister01::especificacao() const
{
    return m_especificacao;
}

int BmfFileRegister01::prazoTermo() const
{
    return m_prazoTermo;
}

std::string BmfFileRegister01::moedaReferencia() const
{
    return m_moedaReferencia;
}

double BmfFileRegister01::precoAbertura() const
{
    return m_precoAbertura;
}

double BmfFileRegister01::precoMaximo() const
{
    return m_precoMaximo;
}

double BmfFileRegister01::precoMinimo() const
{
    return m_precoMinimo;
}

double BmfFileRegister01::precoMedio() const
{
    return m_precoMedio;
}

double BmfFileRegister01::precoFechamento() const
{
    return m_precoFechamento;
}

double BmfFileRegister01::precoMelhorOfertaCompra() const
{
    return m_precoMelhorOfertaCompra;
}

double BmfFileRegister01::precoMelhorOfertaVenda() const
{
    return m_precoMelhorOfertaVenda;
}

int BmfFileRegister01::totalNegociacoes() const
{
    return m_totalNegociacoes;
}

int BmfFileRegister01::totalTitulosNegociados() const
{
    return m_totalTitulosNegociados;
}

double BmfFileRegister01::volumeTotalNegociacoes() const
{
    return m_volumeTotalNegociacoes;
}

double BmfFileRegister01::precoOpcoesTermo() const
{
    return m_precoOpcoesTermo;
}

int BmfFileRegister01::indicadorCorrecaoOpcoesTermo() const
{
    return m_indicadorCorrecaoOpcoesTermo;
}

Date BmfFileRegister01::dataVencimentoOpcoesTermo() const
{
    return m_dataVencimentoOpcoesTermo;
}

int BmfFileRegister01::fatorCorrecaoPapel() const
{
    return m_fatorCorrecaoPapel;
}

double BmfFileRegister01::precoEmPontos() const
{
    return m_precoEmPontos;
}

std::string BmfFileRegister01::codigoIsin() const
{
    return m_codigoIsin;
}

int BmfFileRegister01::numeroDistribuicao() const
{
    return m_numeroDistribuicao;
}

BmfFileRegister01 BmfFileRegister01::fromStream( std::istream & istream )
{
    using namespace StringUtils;

    BmfFileRegister01 reg;

    char c_BMF_DATA_PREGAO[ BMF_DATA_PREGAO + 1 ];
    c_BMF_DATA_PREGAO[ BMF_DATA_PREGAO ] = '\0';
    istream.read( c_BMF_DATA_PREGAO, BMF_DATA_PREGAO );
    reg.m_dataPregao = Date::fromString( c_BMF_DATA_PREGAO, "YYYYMMDD" );

    char c_BMF_CODIGO_BDI[ BMF_CODIGO_BDI + 1 ];
    c_BMF_CODIGO_BDI[ BMF_CODIGO_BDI ] = '\0';
    istream.read( c_BMF_CODIGO_BDI, BMF_CODIGO_BDI );
    reg.m_codigoBdi = trimmed( c_BMF_CODIGO_BDI );

    char c_BMF_CODIGO_NEGOCICAO[ BMF_CODIGO_NEGOCICAO + 1 ];
    c_BMF_CODIGO_NEGOCICAO[ BMF_CODIGO_NEGOCICAO ] = '\0';
    istream.read( c_BMF_CODIGO_NEGOCICAO, BMF_CODIGO_NEGOCICAO );
    reg.m_codigoNegociacao = trimmed( c_BMF_CODIGO_NEGOCICAO );

    char c_BMF_TIPO_MERCADO[ BMF_TIPO_MERCADO + 1 ];
    c_BMF_TIPO_MERCADO[ BMF_TIPO_MERCADO ] = '\0';
    istream.read( c_BMF_TIPO_MERCADO, BMF_TIPO_MERCADO );
    reg.m_tipoMercado = atoi( c_BMF_TIPO_MERCADO );

    char c_BMF_NOME_RESUMIDO[ BMF_NOME_RESUMIDO + 1 ];
    c_BMF_NOME_RESUMIDO[ BMF_NOME_RESUMIDO ] = '\0';
    istream.read( c_BMF_NOME_RESUMIDO, BMF_NOME_RESUMIDO );
    reg.m_nomeResumido = trimmed( c_BMF_NOME_RESUMIDO );

    char c_BMF_ESPECIFICACAO[ BMF_ESPECIFICACAO + 1 ];
    c_BMF_ESPECIFICACAO[ BMF_ESPECIFICACAO ] = '\0';
    istream.read( c_BMF_ESPECIFICACAO, BMF_ESPECIFICACAO );
    reg.m_especificacao = trimmed( c_BMF_ESPECIFICACAO );

    char c_BMF_PRAZO_TERMO[ BMF_PRAZO_TERMO + 1 ];
    c_BMF_PRAZO_TERMO[ BMF_PRAZO_TERMO ] = '\0';
    istream.read( c_BMF_PRAZO_TERMO, BMF_PRAZO_TERMO );
    reg.m_prazoTermo = atoi( c_BMF_PRAZO_TERMO );

    char c_BMF_MOEDA_REFERENCIA[ BMF_MOEDA_REFERENCIA + 1 ];
    c_BMF_MOEDA_REFERENCIA[ BMF_MOEDA_REFERENCIA ] = '\0';
    istream.read( c_BMF_MOEDA_REFERENCIA, BMF_MOEDA_REFERENCIA );
    reg.m_moedaReferencia = trimmed( c_BMF_MOEDA_REFERENCIA );

    char c_BMF_PRECO_ABERTURA[ BMF_PRECO_ABERTURA + 1 ];
    c_BMF_PRECO_ABERTURA[ BMF_PRECO_ABERTURA ] = '\0';
    istream.read( c_BMF_PRECO_ABERTURA, BMF_PRECO_ABERTURA );
    reg.m_precoAbertura = atoi( c_BMF_PRECO_ABERTURA ) / 100.;

    char c_BMF_PRECO_MAXIMO[ BMF_PRECO_MAXIMO + 1 ];
    c_BMF_PRECO_MAXIMO[ BMF_PRECO_MAXIMO ] = '\0';
    istream.read( c_BMF_PRECO_MAXIMO, BMF_PRECO_MAXIMO );
    reg.m_precoMaximo = atoi( c_BMF_PRECO_MAXIMO ) / 100.;

    char c_BMF_PRECO_MINIMO[ BMF_PRECO_MINIMO + 1 ];
    c_BMF_PRECO_MINIMO[ BMF_PRECO_MINIMO ] = '\0';
    istream.read( c_BMF_PRECO_MINIMO, BMF_PRECO_MINIMO );
    reg.m_precoMinimo = atoi( c_BMF_PRECO_MINIMO ) / 100.;

    char c_BMF_PRECO_MEDIO[ BMF_PRECO_MEDIO + 1 ];
    c_BMF_PRECO_MEDIO[ BMF_PRECO_MEDIO ] = '\0';
    istream.read( c_BMF_PRECO_MEDIO, BMF_PRECO_MEDIO );
    reg.m_precoMedio = atoi( c_BMF_PRECO_MEDIO ) / 100.;

    char c_BMF_PRECO_FECHAMENTO[ BMF_PRECO_FECHAMENTO + 1 ];
    c_BMF_PRECO_FECHAMENTO[ BMF_PRECO_FECHAMENTO ] = '\0';
    istream.read( c_BMF_PRECO_FECHAMENTO, BMF_PRECO_FECHAMENTO );
    reg.m_precoFechamento = atoi( c_BMF_PRECO_FECHAMENTO ) / 100.;

    char c_BMF_PRECO_MELHOR_OFERTA_COMPRA[ BMF_PRECO_MELHOR_OFERTA_COMPRA + 1 ];
    c_BMF_PRECO_MELHOR_OFERTA_COMPRA[ BMF_PRECO_MELHOR_OFERTA_COMPRA ] = '\0';
    istream.read( c_BMF_PRECO_MELHOR_OFERTA_COMPRA, BMF_PRECO_MELHOR_OFERTA_COMPRA );
    reg.m_precoMelhorOfertaCompra = atoi( c_BMF_PRECO_MELHOR_OFERTA_COMPRA ) / 100.;

    char c_BMF_PRECO_MELHOR_OFERTA_VENDA[ BMF_PRECO_MELHOR_OFERTA_VENDA + 1 ];
    c_BMF_PRECO_MELHOR_OFERTA_VENDA[ BMF_PRECO_MELHOR_OFERTA_VENDA ] = '\0';
    istream.read( c_BMF_PRECO_MELHOR_OFERTA_VENDA, BMF_PRECO_MELHOR_OFERTA_VENDA );
    reg.m_precoMelhorOfertaVenda = atoi( c_BMF_PRECO_MELHOR_OFERTA_VENDA ) / 100.;

    char c_BMF_TOTAL_NEGOCIACOES[ BMF_TOTAL_NEGOCIACOES + 1 ];
    c_BMF_TOTAL_NEGOCIACOES[ BMF_TOTAL_NEGOCIACOES ] = '\0';
    istream.read( c_BMF_TOTAL_NEGOCIACOES, BMF_TOTAL_NEGOCIACOES );
    reg.m_totalNegociacoes = atoi( c_BMF_TOTAL_NEGOCIACOES );

    char c_BMF_QUANTIDADE_TITULOS_NEGOCIADOS[ BMF_QUANTIDADE_TITULOS_NEGOCIADOS + 1 ];
    c_BMF_QUANTIDADE_TITULOS_NEGOCIADOS[ BMF_QUANTIDADE_TITULOS_NEGOCIADOS ] = '\0';
    istream.read( c_BMF_QUANTIDADE_TITULOS_NEGOCIADOS, BMF_QUANTIDADE_TITULOS_NEGOCIADOS );
    reg.m_totalTitulosNegociados = atoi( c_BMF_QUANTIDADE_TITULOS_NEGOCIADOS );

    char c_BMF_VOLUME_TITULOS_NEGOCIADOS[ BMF_VOLUME_TITULOS_NEGOCIADOS + 1 ];
    c_BMF_VOLUME_TITULOS_NEGOCIADOS[ BMF_VOLUME_TITULOS_NEGOCIADOS ] = '\0';
    istream.read( c_BMF_VOLUME_TITULOS_NEGOCIADOS, BMF_VOLUME_TITULOS_NEGOCIADOS );
    reg.m_volumeTotalNegociacoes = atoi( c_BMF_VOLUME_TITULOS_NEGOCIADOS ) / 100.;

    char c_BMF_PRECO_OPCOES_TERMO[ BMF_PRECO_OPCOES_TERMO + 1 ];
    c_BMF_PRECO_OPCOES_TERMO[ BMF_PRECO_OPCOES_TERMO ] = '\0';
    istream.read( c_BMF_PRECO_OPCOES_TERMO, BMF_PRECO_OPCOES_TERMO );
    reg.m_precoOpcoesTermo = atoi( c_BMF_PRECO_OPCOES_TERMO ) / 100.;

    char c_BMF_CORRECAO_OPCOES_TERMO[ BMF_CORRECAO_OPCOES_TERMO + 1 ];
    c_BMF_CORRECAO_OPCOES_TERMO[ BMF_CORRECAO_OPCOES_TERMO ] = '\0';
    istream.read( c_BMF_CORRECAO_OPCOES_TERMO, BMF_CORRECAO_OPCOES_TERMO );
    reg.m_indicadorCorrecaoOpcoesTermo = atoi( c_BMF_CORRECAO_OPCOES_TERMO );

    char c_BMF_DATA_VENC_OPCOES_TERMO[ BMF_DATA_VENC_OPCOES_TERMO + 1 ];
    c_BMF_DATA_VENC_OPCOES_TERMO[ BMF_DATA_VENC_OPCOES_TERMO ] = '\0';
    istream.read( c_BMF_DATA_VENC_OPCOES_TERMO, BMF_DATA_VENC_OPCOES_TERMO );
    reg.m_dataVencimentoOpcoesTermo = Date::fromString( c_BMF_DATA_VENC_OPCOES_TERMO, "YYYYMMAA" );

    char c_BMF_FATOR_COTACAO[ BMF_FATOR_COTACAO + 1 ];
    c_BMF_FATOR_COTACAO[ BMF_FATOR_COTACAO ] = '\0';
    istream.read( c_BMF_FATOR_COTACAO, BMF_FATOR_COTACAO );
    reg.m_fatorCorrecaoPapel = atoi( c_BMF_FATOR_COTACAO );

    char c_BMF_PRECO_PONTOS[ BMF_PRECO_PONTOS + 1 ];
    c_BMF_PRECO_PONTOS[ BMF_PRECO_PONTOS ] = '\0';
    istream.read( c_BMF_PRECO_PONTOS, BMF_PRECO_PONTOS );
    reg.m_precoEmPontos = atoi( c_BMF_PRECO_PONTOS ) / 1000000.00 /* a million */;

    char c_BMF_CODIGO_ISIN[ BMF_CODIGO_ISIN + 1 ];
    c_BMF_CODIGO_ISIN[ BMF_CODIGO_ISIN ] = '\0';
    istream.read( c_BMF_CODIGO_ISIN, BMF_CODIGO_ISIN );
    reg.m_codigoIsin = trimmed( c_BMF_CODIGO_ISIN );

    char c_BMF_NUMERO_DISTRIBUICAO[ BMF_NUMERO_DISTRIBUICAO + 1 ];
    c_BMF_NUMERO_DISTRIBUICAO[ BMF_NUMERO_DISTRIBUICAO ] = '\0';
    istream.read( c_BMF_NUMERO_DISTRIBUICAO, BMF_NUMERO_DISTRIBUICAO );
    reg.m_numeroDistribuicao = atoi( c_BMF_NUMERO_DISTRIBUICAO );

    return reg;
}

std::ostream & operator<<( std::ostream & o, const BmfFileRegister01 & rhs )
{
    o <<
            "DATA_PREGAO:                   " << rhs.m_dataPregao.toString( "DD/MM/YYYY" )    << std::endl <<
            "CODIGO_BDI:                    " << rhs.m_codigoBdi                      << std::endl <<
            "CODIGO_NEGOCICAO:              " << rhs.m_codigoNegociacao               << std::endl <<
            "TIPO_MERCADO:                  " << rhs.m_tipoMercado                    << std::endl <<
            "NOME_RESUMIDO:                 " << rhs.m_nomeResumido                   << std::endl <<
            "ESPECIFICAÇÃO:                 " << rhs.m_especificacao                  << std::endl <<
            "PRAZO_TERMO:                   " << rhs.m_prazoTermo                     << std::endl <<
            "MOEDA_REFERENCIA               " << rhs.m_moedaReferencia                << std::endl <<
            "PRECO_ABERTURA:                " << rhs.m_precoAbertura                  << std::endl <<
            "PRECO_MAXIMO:                  " << rhs.m_precoMaximo                    << std::endl <<
            "PRECO_MINIMO:                  " << rhs.m_precoMinimo                    << std::endl <<
            "PRECO_MEDIO:                   " << rhs.m_precoMedio                     << std::endl <<
            "PRECO_FECHAMENTO:              " << rhs.m_precoFechamento                << std::endl <<
            "PRECO_MELHOR_OFERTA_COMPRA:    " << rhs.m_precoMelhorOfertaCompra        << std::endl <<
            "PRECO_MELHOR_OFERTA_VENDA:     " << rhs.m_precoMelhorOfertaVenda         << std::endl <<
            "TOTAL_NEGOCIACÕES:             " << rhs.m_totalNegociacoes               << std::endl <<
            "TOTAL_TITULOS_NEGOCIADOS:      " << rhs.m_totalTitulosNegociados         << std::endl <<
            "VOLUME_TOTAL_NEGOCIACOES:      " << rhs.m_volumeTotalNegociacoes         << std::endl <<
            "PRECO_OPCOES_TERMO:            " << rhs.m_precoOpcoesTermo               << std::endl <<
            "CORRECAO_OPCOES_TERMO:         " << rhs.m_indicadorCorrecaoOpcoesTermo   << std::endl <<
            "DATA_VENCIMENTO_OPCOES_TERMO:  " << rhs.m_dataVencimentoOpcoesTermo.toString( "DD/MM/YYYY" ) << std::endl <<
            "FATOR_CORRECAO_PAPEL:          " << rhs.m_fatorCorrecaoPapel             << std::endl <<
            "PRECO_PONTOS:                  " << rhs.m_precoEmPontos                  << std::endl <<
            "CODIGO_ISIN:                   " << rhs.m_codigoIsin                     << std::endl <<
            "NUMERO_DISTRIBUICAO:           " << rhs.m_numeroDistribuicao             << std::endl;

    return o;
}
