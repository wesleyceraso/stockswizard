#ifndef BMFFILEREGISTER01_H
#define BMFFILEREGISTER01_H

#include <string>
#include <ostream>
#include <date.h>

class BmfFileRegister01
{
public:
    BmfFileRegister01();

    Date        dataPregao() const;
    std::string codigoBdi() const;
    std::string codigoNegociacao() const;
    int         tipoMercado() const;
    std::string nomeResumido() const;
    std::string especificacao() const;
    int         prazoTermo() const;
    std::string moedaReferencia() const;
    double      precoAbertura() const;
    double      precoMaximo() const;
    double      precoMinimo() const;
    double      precoMedio() const;
    double      precoFechamento() const;
    double      precoMelhorOfertaCompra() const;
    double      precoMelhorOfertaVenda() const;
    int         totalNegociacoes() const;
    int         totalTitulosNegociados() const;
    double      volumeTotalNegociacoes() const;
    double      precoOpcoesTermo() const;
    int         indicadorCorrecaoOpcoesTermo() const;
    Date        dataVencimentoOpcoesTermo() const;
    int         fatorCorrecaoPapel() const;
    double      precoEmPontos() const;
    std::string codigoIsin() const;
    int         numeroDistribuicao() const;

    static BmfFileRegister01 fromStream( std::istream & istream );

    friend std::ostream & operator<<( std::ostream & o, const BmfFileRegister01 & rhs );

private:
    Date        m_dataPregao;
    std::string m_codigoBdi;
    std::string m_codigoNegociacao;
    int         m_tipoMercado;
    std::string m_nomeResumido;
    std::string m_especificacao;
    int         m_prazoTermo;
    std::string m_moedaReferencia;
    double      m_precoAbertura;
    double      m_precoMaximo;
    double      m_precoMinimo;
    double      m_precoMedio;
    double      m_precoFechamento;
    double      m_precoMelhorOfertaCompra;
    double      m_precoMelhorOfertaVenda;
    int         m_totalNegociacoes;
    int         m_totalTitulosNegociados;
    double      m_volumeTotalNegociacoes;
    double      m_precoOpcoesTermo;
    int         m_indicadorCorrecaoOpcoesTermo;
    Date        m_dataVencimentoOpcoesTermo;
    int         m_fatorCorrecaoPapel;
    double      m_precoEmPontos;
    std::string m_codigoIsin;
    int         m_numeroDistribuicao;
};

#endif // BMFFILEREGISTER01_H
