#include "tradingday.h"

TradingDay::TradingDay( const Date & date ):
    m_date( date )
{
}

Date TradingDay::date() const
{
    return m_date;
}

bool TradingDay::operator==( const TradingDay & rhs ) const
{
    return m_date == rhs.m_date;
}

bool TradingDay::operator<(const TradingDay & rhs) const
{
    return m_date < rhs.m_date;
}

std::ostream & operator<<( std::ostream & o, const TradingDay & rhs )
{
    o <<
            "Data: " << rhs.m_date.toString( "DD/MM/YYYY" ) << std::endl;
    return o;
}
