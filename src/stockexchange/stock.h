#ifndef STOCK_H
#define STOCK_H

#include <string>
#include <ostream>

class Stock
{
public:
    Stock( const std::string & code, const std::string & name, const std::string & specification, int correctionFactor );

    std::string code() const;
    std::string name() const;
    std::string specification() const;
    int         correctionFactor() const;

    bool operator==( const Stock & ) const;
    bool operator<( const Stock & ) const;

    friend std::ostream & operator<<( std::ostream &, const Stock & );

private:
    std::string m_code;
    std::string m_name;
    std::string m_specification;
    int         m_correctionFactor;

};

#endif // STOCK_H
