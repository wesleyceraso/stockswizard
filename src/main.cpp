#include <QApplication>
#include <QtQuick/QQuickWindow>
#include <QtQml/QQmlApplicationEngine>
#include <QQmlContext>

#include "stockanalysis/gui/stockanalysiswindow.h"

#include <iostream>
#include <iomanip>
#include "stockexchange/bmfstockexchange.h"
#include "stockexchange/tradingday.h"
#include "../../utils/utils.h"
#include <algorithm>

int main( int argc, char * argv[] )
{
    QApplication app( argc, argv );

    StockAnalysisWindow presentation;

    QQmlApplicationEngine e;
    e.rootContext()->setContextProperty("presentation", &presentation);
    e.load("../src/stockanalysis/gui/main.qml");
    static_cast<QQuickWindow*>(e.rootObjects().value(0))->show();

    return app.exec();

    return 0;
}
