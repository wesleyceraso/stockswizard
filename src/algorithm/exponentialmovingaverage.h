#ifndef EXPONENTIALMOVINGAVERAGE_H
#define EXPONENTIALMOVINGAVERAGE_H

#include <vector>
#include <cassert>
#include "movingaverage.h"
#include <utils.h>
#include <iostream>

class ExponentialMovingAverage
{
public:
    template <class T>
    static std::vector<T> process(const std::vector<T> & vector, unsigned int period)
    {
        assert(period <= vector.size());

        T average = MovingAverage::process(subVector(vector, 0, period), period).front();
        std::vector<T> averages;
        averages.push_back(average);

        T k = 2. / (period + 1);

        for (int i=period; i<vector.size(); ++i)
        {
            average = vector[i] * k + average * (1. - k);
            averages.push_back(average);
        }

        return averages;
    }
};

#endif // EXPONENTIALMOVINGAVERAGE_H
