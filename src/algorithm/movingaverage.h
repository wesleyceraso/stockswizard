#ifndef MOVINGAVERAGE_H
#define MOVINGAVERAGE_H

#include <vector>
#include <cassert>

class MovingAverage
{
public:
    template <class T>
    static std::vector<T> process(const std::vector<T> & vector, unsigned int period)
    {
        assert(period <= vector.size());

        std::vector<T> averages;

        typename std::vector<T>::const_iterator behind = vector.begin();
        typename std::vector<T>::const_iterator current = vector.begin();
        T sum = 0;

        for (unsigned int decShift = period; decShift--;)
        {
            sum += *(current++);
        }
        T average = sum / period;
        averages.push_back(average);

        while (current != vector.end())
        {
            sum -= *(behind++);
            sum += *(current++);

            T average = sum / period;
            averages.push_back(average);
        }

        return averages;
    }
};

#endif // MOVINGAVERAGE_H
